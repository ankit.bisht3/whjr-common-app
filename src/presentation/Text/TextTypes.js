import Color from '../../common/Colors';
import { FontName, FontSizeHeading, FontSizeBody } from '../../utility/FontsUtil';

const TextTypes = {
    headingOneText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingOne,
        fontFamily: FontName.whjBold,
    },
    headingTwoText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingTwo,
        fontFamily: FontName.whjBold,
    },
    headingThreeText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingThree,
        fontFamily: FontName.whjBold,
    },
    headingFourText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingFour,
        fontFamily: FontName.whjBold,
    },
    headingOneDemiText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyOne,
        fontFamily: FontName.whjDemi,
    },
    bodyOneDemiText: {
        color: Color.whiteWhj,
        fontSize: FontSizeHeading.headingOne,
        fontFamily: FontName.whjDemi,
    },
    bodyTwoDemiText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyTwo,
        fontFamily: FontName.whjDemi,
    },
    bodyThreeDemiText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyThree,
        fontFamily: FontName.whjDemi,
        backgroundColor: Color.greenLightLime,
    },
    bodyThreeDemiWhiteText: {
        color: Color.whiteWhj,
        fontSize: FontSizeBody.bodyThree,
        fontFamily: FontName.whjDemi,
    },
    bodyFourDemiText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyFour,
        fontFamily: FontName.whjDemi,
    },
    bodyOneMediumText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyOne,
        fontFamily: FontName.whjMedium,
    },
    bodyTwoMediumText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyTwo,
        fontFamily: FontName.whjMedium,
    },
    bodyThreeMediumText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyThree,
        fontFamily: FontName.whjMedium,
    },
    bodyFourMediumText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyFour,
        fontFamily: FontName.whjMedium,
    },
    bodyOneBookText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyOne,
        fontFamily: FontName.whjBook,
    },
    bodyTwoBookText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyTwo,
        fontFamily: FontName.whjBook,
    },
    bodyThreeBookText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyThree,
        fontFamily: FontName.whjBook,
    },
    bodyFourBookText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyFour,
        fontFamily: FontName.whjBook,
    },
    bodyFiveBookText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyFive,
        fontFamily: FontName.whjBook,
    },
    bodyOneMediumBlackText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeBody.bodyOne,
        fontFamily: FontName.whjMedium,
    },
    bodyOneBlackText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeBody.bodyOne,
        fontFamily: FontName.whjDemi,
    },
    bodyTwoMediumBlackText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeBody.bodyTwo,
        fontFamily: FontName.whjMedium,
    },
    bodyThreeMediumBlackText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeBody.bodyThree,
        fontFamily: FontName.whjMedium,
    },
    bodyThreeMediumWhiteText: {
        color: Color.whiteWhj,
        fontSize: FontSizeBody.bodyThree,
        fontFamily: FontName.whjMedium,
    },
    bodyFourMediumBlackText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeBody.bodyFour,
        fontFamily: FontName.whjMedium,
    },
    bodyFourMediumWhiteText: {
        color: Color.whiteWhj,
        fontSize: FontSizeBody.bodyFour,
        fontFamily: FontName.whjMedium,
    },
    bodyOneLightText: {
        color: Color.lightBlackTextWhj,
        fontSize: FontSizeBody.bodyOne,
        fontFamily: FontName.whjBook,
    },
    bodyTwoLightText: {
        color: Color.lightBlackTextWhj,
        fontSize: FontSizeBody.bodyTwo,
        fontFamily: FontName.whjBook,
    },
    bodyThreeLightText: {
        color: Color.lightBlackTextWhj,
        fontSize: FontSizeBody.bodyThree,
        fontFamily: FontName.whjBook,
    },
    bodyFourLightText: {
        color: Color.lightBlackTextWhj,
        fontSize: FontSizeBody.bodyFour,
        fontFamily: FontName.whjBook,
    },
    bodyFiveLightText: {
        color: Color.greyDarkWhj,
        fontSize: FontSizeBody.bodyFive,
        fontFamily: FontName.whjMedium,
    },
    headingFiveText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingFive,
        fontFamily: FontName.whjBold,
    },
    headingSixText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingSix,
        fontFamily: FontName.whjBold,
    },
    bodyFiveMediumText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeBody.bodyFive,
        fontFamily: FontName.whjMedium,
    },
    headingLargeText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingLarge,
        fontFamily: FontName.whjBold,
    },
    headingLargerText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingLarger,
        fontFamily: FontName.whjBold,
    },
    headingExtraLargeText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingExtraLarge,
        fontFamily: FontName.whjBold,
    },
    bodyExtraLargeText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeBody.bodySix,
        fontFamily: FontName.whjDemi,
    },
    bodyMediumSixText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeBody.bodySix,
        fontFamily: FontName.whjMedium,
    },
    bodyBoldTwoText: {
        color: Color.whiteWhj,
        fontSize: FontSizeBody.bodyTwo,
        fontFamily: FontName.whjBold,
    },
    bodyOneText: {
        color: Color.greyNew,
        fontSize: FontSizeBody.bodyTwo,
        fontFamily: FontName.whjBook,
    },
    bodySmallHeadingDarkText: {
        color: Color.blackAlternate,
        fontSize: FontSizeBody.bodyTwo,
        fontFamily: FontName.whjMedium,
    },
    bodySmallOptionText: {
        color: Color.selectCourseColor,
        fontSize: FontSizeBody.bodyThree,
        fontFamily: FontName.whjMedium,
    },
    headingTwoBlackDemiText: {
        color: Color.blackTextWhj,
        fontSize: FontSizeHeading.headingTwo,
        fontFamily: FontName.whjDemi,
    },
};

export default TextTypes;
