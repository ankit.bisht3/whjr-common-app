import React from 'react';
import { View } from 'react-native';
import LottieView from 'lottie-react-native';
import { styles } from './styles';
import { LoaderImages } from '../../common/Images';

const Loader = () => {
    return (
        <View style={styles.loaderContainer}>
            <View style={styles.loaderView}>
                <LottieView
                    style={styles.genericLoaderStyle}
                    source={LoaderImages.GenericLoader}
                    resizeMode="cover"
                    autoPlay
                />
            </View>
        </View>
    );
};

export default Loader;
