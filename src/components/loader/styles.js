import { StyleSheet } from 'react-native';
import Colors from '../../common/Colors';
import { SCREEN_WIDTH } from '../../utility/SizeUtil';

export const styles = StyleSheet.create({
    loaderView: {
        height: 125,
        width: 125,
        alignSelf: 'center',
    },
    loaderContainer: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        top: 0,
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.loaderTransparent,
    },
    genericLoaderStyle: {
        width: SCREEN_WIDTH * 0.1,
        aspectRatio: 1,
        flexGrow: 1,
        alignSelf: 'center',
    }
})