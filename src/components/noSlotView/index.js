import React from 'react';
import {View, ImageBackground} from 'react-native';
import PhoneStyles from './style/PhoneStyles';
import localisation from '../../localisation';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import { ScheduleScreenImage } from '../../common/Images';

const NoSlotView = (props) => {
  const {text} = props;
  return (
    <View style={PhoneStyles.noSlotAvailableStyles}>
      <ImageBackground
        source={ScheduleScreenImage.blackWhiteStrips}
        style={PhoneStyles.noSlotBgImageStyles}>
        <TextWhj
          title={text ? text : localisation.noSlotAvailable}
          type={TextTypes.bodyFourMediumText}
          style={PhoneStyles.noSlotAvailableFontStyles}
        />
      </ImageBackground>
    </View>
  );
};

export default NoSlotView;
