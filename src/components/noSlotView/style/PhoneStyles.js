import {StyleSheet} from 'react-native';
import Colors from '../../../common/Colors';

const PhoneStyles = StyleSheet.create({
  noSlotAvailableStyles: {
    alignSelf: 'stretch',
    // marginVertical: 12,
    // marginBottom: 12,
    marginVertical: 8,
    height: 40,
    // marginLeft: 4,
    textAlign: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    overflow: 'hidden',
    borderColor: Colors.noSlotBorderColor,
    borderWidth: 0.5,
  },
  noSlotBgImageStyles: {
    flex: 1,
    marginLeft: -2,
    height: 40,
    resizeMode: 'cover',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  noSlotAvailableFontStyles: {
    color: Colors.disableBtnTextColor,
    lineHeight: 16,
  },
});

export default PhoneStyles;
