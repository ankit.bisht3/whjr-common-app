import React, {Component} from 'react';
import {
  Modal,
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';
import {hasNotch} from 'react-native-device-info';
import localisation from '../../localisation';
import ButtonTypes from '../../presentation/Button/ButtonTypes';
import ButtonWhj from '../../presentation/Button';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import ImageWhj from '../../presentation/Image';
import { ClassPopupsImages } from '../../common/Images';

class Covid19LockdownPopup extends Component {
  render() {
    const {closeModal, maxClassesAllowedInWeek} = this.props;
    return (
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          onRequestClose={() => closeModal()}>
          <View
            style={styles.contentContainer}
            onStartShouldSetResponder={() => {
              closeModal();
              return true;
            }}>
            <TouchableOpacity
              onPress={() => closeModal()}
              style={[styles.closeButtonStyle, {top: hasNotch() ? 32 : 25}]}>
              <ImageWhj
                imageData={{
                  source: ClassPopupsImages.closeDialog,
                  style: styles.closeIconStyle,
                }}
              />
            </TouchableOpacity>
            <View
              style={styles.modalView}
              onStartShouldSetResponder={() => true}>
              <View style={styles.rectangleImage}>
                <Image
                  style={styles.bannerImage}
                  source={ClassPopupsImages.covidImage}
                  resizeMode={'contain'}
                />
              </View>
              <View style={{paddingHorizontal: 8}}>
                <View style={styles.headerStyles}>
                  <TextWhj
                    title={localisation.covidPopupHeader(
                      maxClassesAllowedInWeek,
                    )}
                    type={TextTypes.headingFourText}
                    style={styles.headerTextStyles}
                  />
                </View>
                <View style={styles.marginTopDist}>
                  <TextWhj
                    title={localisation.covidPopupDescription(
                      maxClassesAllowedInWeek,
                    )}
                    type={TextTypes.bodyThreeBookText}
                    style={styles.textStyles}
                  />
                </View>
                <View style={styles.marginTopDist}>
                  <TextWhj
                    title={localisation.covidPopupFooter}
                    type={TextTypes.bodyThreeMediumBlackText}
                    style={styles.textStyles}
                  />
                </View>
                <ButtonWhj
                  onPress={() => {
                    closeModal();
                  }}
                  title={localisation.covidPopupBtnText}
                  width="100%"
                  type={ButtonTypes.orangeBigButton}
                  style={styles.btnContainer}
                />
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerStyles: {
    marginBottom: 8,
    marginTop: 16,
    alignSelf: 'flex-start',
  },
  headerTextStyles: {
    color: '#FC5A5A',
  },
  btnContainer: {
    flexDirection: 'row',
    marginBottom: 8,
  },
  textStyles: {
    color: '#222222',
  },
  marginTopDist: {
    marginBottom: 16,
  },
  bannerImage: {
    height: 122,
    width: 296,
    alignSelf: 'center',
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    height: '100%',
    width: '100%',
  },
  centeredView: {
    flex: 1,
    marginTop: 22,
  },
  modalView: {
    marginVertical: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    paddingVertical: 16,
    paddingHorizontal: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginHorizontal: 24,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  closeButtonStyle: {
    alignSelf: 'flex-end',
    position: 'absolute',
    top: 0,
    marginTop: 20,
  },
  closeIconStyle: {
    width: 18,
    height: 18,
    marginRight: 20,
  },
  rectangleImage: {width: '100%', height: 122, backgroundColor: '#FFFAF4'},
});

export default Covid19LockdownPopup;
