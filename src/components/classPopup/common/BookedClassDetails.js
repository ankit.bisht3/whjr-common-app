import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Localisation from '../../../localisation';
import TextWhj from '../../../presentation/Text';
import TextTypes from '../../../presentation/Text/TextTypes';
import Styles from '../styles';
import { ClassPopupsImages } from '../../../common/Images';
import { camelCase } from '../../../utility';

const BookedClassDetails = (props) => {
    const { classData, isSubstitute, showDefaultTeacher } = props;
    return (
        <>
            <View style={Styles.bodyContent}>
                <Image
                    style={Styles.styleCalendar}
                    source={ClassPopupsImages.blackCalendar}
                />
                <View style={Styles.bodyContainer}>
                    <TextWhj
                        title={Localisation.popupDate}
                        type={TextTypes.bodyThreeBookText}>
                        {'\n'}
                        <TextWhj
                            title={classData?.classDate}
                            type={TextTypes.bodyTwoMediumBlackText}
                        />
                    </TextWhj>
                </View>
            </View>
            <View style={Styles.bodyContent}>
                <Image
                    style={Styles.styleClock}
                    source={ClassPopupsImages.blackClock}
                />
                <View style={Styles.bodyContainer}>
                    <TextWhj
                        title={Localisation.popupTime}
                        type={TextTypes.bodyThreeBookText}>
                        {'\n'}
                        <TextWhj
                            title={classData?.classTime}
                            type={TextTypes.bodyTwoMediumBlackText}
                        />
                    </TextWhj>
                </View>
            </View>
            <View style={Styles.bodyContent}>
                <Image
                    style={Styles.styleTeacher}
                    source={ClassPopupsImages.teacherIcon}
                />
                <View style={Styles.teacherContainer}>
                    <TextWhj
                        title={Localisation.popupTeacher}
                        style={Styles.substituteTeacherTitleStyle}
                        type={TextTypes.bodyThreeBookText}
                    />
                    <TouchableOpacity
                        style={Styles.bodyTeacherContainer}
                        onPress={() => props.onSelectTeacher()}>
                        <TextWhj
                            title={camelCase(classData?.classTeacher)}
                            type={TextTypes.bodyTwoMediumBlackText}
                            style={Styles.bodyTeacherContainerText}
                            numberOfLines={2}
                        />
                        {(isSubstitute || showDefaultTeacher) && (
                            <View style={Styles.substituteTeacherContainer}>
                                <TextWhj
                                    title={
                                        isSubstitute
                                            ? Localisation.substituteText
                                            : Localisation.yourTeacher
                                    }
                                    type={TextTypes.bodyThreeMediumBlackText}
                                    style={Styles.substituteTeacherText}
                                />
                            </View>
                        )}
                    </TouchableOpacity>
                </View>
            </View>
            {props?.isTeacherEmail && (
                <View style={Styles.bodyContent}>
                    <Image
                        style={Styles.styleEmail}
                        source={ClassPopupsImages.blackEmail}
                    />
                    <View style={Styles.bodyContainer}>
                        <TextWhj
                            style={Styles.teacherEmail}
                            title={Localisation.popupTeacherEmail}
                            type={TextTypes.bodyThreeBookText}>
                            {'\n'}
                            <TextWhj
                                title={classData?.classTeacherEmail}
                                type={TextTypes.bodyTwoMediumBlackText}
                            />
                        </TextWhj>
                    </View>
                </View>
            )}
        </>
    );
};

BookedClassDetails.propTypes = {
    onSelectTeacher: PropTypes.func,
    isSubstitute: PropTypes.bool,
};

BookedClassDetails.defaultProps = {
    onSelectTeacher: () => { },
    isSubstitute: false,
};

export default BookedClassDetails;
