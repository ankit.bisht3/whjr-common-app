import React, {Component} from 'react';
import {View, Image, Icon, TouchableOpacity} from 'react-native';
import Localisation from '../../localisation';
import ModalWhj from '../../presentation/Modal/ModalWhj';
import ButtonTypes from '../../presentation/Button/ButtonTypes';
import ButtonWhj from '../../presentation/Button';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
import Styles from './styles';
import { CommonImages } from '../../common/Images';
import Common from '../../localisation';
import {camelCase} from '../../utility';

class SelectTeacher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTeacher: null,
    };
  }

  _selectTeacher = (teacher) => {
    this.setState({
      selectedTeacher: teacher,
    });
  };

  render() {
    const {teacherList, selectedTeacher} = this.props;
    const currentSelectedTeacher =
      this.state.selectedTeacher || selectedTeacher;

    return (
      <ModalWhj
        style={Styles.popupStyle}
        transparent={true}
        animationType={'fade'}
        setActionModalVisible={(value) =>
          this.props.setActionModalVisible(value)
        }>
        <View style={Styles.headerContainerStyle}>
          <View style={Styles.selectTeacherHeaderContainer}>
            <TextWhj
              title={Common.selectTeacher}
              type={TextTypes.bodyOneBlackText}
            />
          </View>
        </View>
        <View>
          {teacherList?.map((teacher, index) => {
            return (
              <TouchableOpacity
                onPress={() => this._selectTeacher(teacher)}
                key={index}
                style={Styles.listContainer}>
                <View style={Styles.listItem}>
                  <TextWhj
                    title={camelCase(teacher.name)}
                    type={TextTypes.bodyTwoBookText}
                    style={Styles.itemText}
                  />
                </View>
                {this.props.defaultTeacherId === teacher.id ? (
                  <View style={Styles.defaultTeacherContainer}>
                    <TextWhj
                      title={Localisation.primaryTeacher}
                      type={TextTypes.bodyThreeMediumBlackText}
                      style={Styles.defaultTeacherText}
                    />
                  </View>
                ) : (
                  <View style={Styles.substituteTeacherContainer}>
                    <TextWhj
                      title={Localisation.substituteText}
                      type={TextTypes.bodyThreeMediumBlackText}
                      style={Styles.substituteTeacherText}
                    />
                  </View>
                )}
                {currentSelectedTeacher.id === teacher.id ? (
                  <View style={Styles.radioContainer}>
                    <Image
                      source={CommonImages.orangeTick}
                      style={Styles.tickImage}
                      resizeMode="contain"
                    />
                  </View>
                ) : (
                  <View style={Styles.selectedRadioButton}>
                    <View style={Styles.radioButton} />
                  </View>
                )}
              </TouchableOpacity>
            );
          })}
        </View>
        <View style={Styles.upcomingBottomStyle}>
          <ButtonWhj
            onPress={() => {
              this.props.selectTeacher(currentSelectedTeacher);
            }}
            title={Common.continue}
            style={Styles.selectTeacherCTA}
            type={ButtonTypes.purpleMediumButton}
          />
        </View>
      </ModalWhj>
    );
  }
}

SelectTeacher.propTypes = {};

export default SelectTeacher;
