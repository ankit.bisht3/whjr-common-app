import { StyleSheet } from 'react-native';
import Colors from '../../../common/Colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../utility/SizeUtil';

const Styles = StyleSheet.create({
    popupStyle: {},
    spotlightContainer: {
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 16,
        backgroundColor: Colors.darkSkyblue,
        borderTopLeftRadius: 19,
        borderTopRightRadius: 19,
    },
    ptmTextColor: { color: Colors.whiteWhj, alignSelf: 'flex-start' },
    spotlightTagContainer: {
        flexDirection: 'row',
        backgroundColor: Colors.darkSkyblue,
        height: 36,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        paddingHorizontal: 16,
    },
    spotLightIcon: {
        height: 20,
        width: 20,
        alignSelf: 'center',
    },
    spotlightText: { color: Colors.whiteWhj, paddingLeft: 18 },
    spotlightIcon: { height: 45, width: 55 },
    spotlightTextContainer: { marginLeft: 14, paddingRight: 50 },
    summaryClassPopupStyle: {
        height: SCREEN_HEIGHT * 0.5,
    },
    cancelledClassPopupStyle: {
        height: SCREEN_HEIGHT * 0.45,
    },
    scrollContainerStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
    scrollStyle: {
        flex: 1,
    },
    marginHorizontal16: {
        marginHorizontal: 16,
    },
    marginTop16: {
        marginTop: 16,
    },
    greyRule: {
        borderBottomColor: Colors.greyPopupBorderColor,
        borderBottomWidth: 1,
    },
    marginHorizontal8: {
        marginHorizontal: 8,
    },
    classConfirmedPopup: {
        margin: 0,
        justifyContent: 'flex-end',
    },
    classConfirmedPopupContianer: {
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: Colors.whiteWhj,
        paddingHorizontal: 24,
        paddingTop: 8,
        justifyContent: 'flex-end',
    },
    headerContainerStyle: {
        borderBottomColor: Colors.greyPopupBorderColor,
        flex: 1,
        justifyContent: 'center',
        minHeight: 52,
    },
    headerSummaryContainerStyle: {
        marginBottom: 16,
        flex: 1,
        justifyContent: 'center',
        minHeight: 52,
    },
    badgeContainerStyle: {
        flex: 1,
        justifyContent: 'center',
    },
    cancelledHeaderContainerStyle: {
        paddingBottom: 16,
        marginBottom: 16,
        flex: 1,
        minHeight: 45,
    },
    cancelledGeaderContainerStyle: {
        borderBottomColor: Colors.greyPopupBorderColor,
        borderBottomWidth: 1,
        marginBottom: 16,
    },
    headerImage: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    loseCreditTextStyle: {
        color: Colors.redWhj,
    },
    headerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 16,
    },
    cancelledHeaderContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: Colors.greyPopupBorderColor,
        borderBottomWidth: 1,
        paddingBottom: 16,
    },
    classSummaryStyleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        // paddingBottom: 16,
        width: SCREEN_WIDTH - 32,
        backgroundColor: 'white',
    },
    modelContainerStyle: { padding: 0 },
    classSummaryHeaderOuterStyleContainer: {
        backgroundColor: 'white',
        width: SCREEN_WIDTH,
        height: 68,
        justifyContent: 'center',
    },
    summmaryClassHeaderContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 16,
        marginBottom: 20,
        shadowRadius: 20,
        shadowOpacity: 0.2,
        shadowOffset: {
            width: 4,
            height: 10,
        },
        shadowColor: Colors.blackWhj,
    },
    summmaryClassHeaderContainerShadow: {
        backgroundColor: 'red',
        width: 300,
        height: 20,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.12,
        shadowRadius: 60,
    },
    imageBackgroud: {
        borderRadius: 4,
        width: 36,
        height: 36,
        justifyContent: 'center',
        marginRight: 12,
    },
    techIconStyle: {
        width: 17,
        height: 11,
        alignSelf: 'center',
    },
    BoosterIcon: {
        height: 33,
        width: 36,
        marginRight: 12,
    },
    uncheckedBoxStyle: {
        width: 20,
        height: 20,
        alignSelf: 'center',
        resizeMode: 'contain',
        marginRight: 18,
    },
    classConfirmedEmojiStyle: {
        height: 80,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    scheduleClassConfirmedEmojiStyle: {
        height: 100,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    yayClassScheduledStyle: {
        color: Colors.greenWhj,
        marginTop: 12,
    },
    yayClassEmojiTextScheduled: {
        color: Colors.greenWhj,
        marginTop: -8,
        textAlign: 'center',
    },
    slotConfirmedStyle: {
        textAlign: 'center',
        marginTop: 4,
    },
    confirmationBoxStyle: {
        flexDirection: 'row',
        marginTop: 16,
        alignItems: 'center',
    },
    notCompletedBottomContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: Colors.pinkLightWhj,
        borderRadius: 24,
        width: '100%',
        paddingLeft: 20,
        paddingVertical: 8,
        marginVertical: 17,
    },
    completedBottomContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'flex-start',
        backgroundColor: Colors.greenLightWhj,
        borderRadius: 24,
        paddingLeft: 20,
        paddingVertical: 8,
        marginVertical: 17,
        width: SCREEN_WIDTH - 16,
    },
    classSummaryDescriptionStyle: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        justifyContent: 'flex-start',
        backgroundColor: Colors.pinkLightWhj,
        borderRadius: 24,
        width: '100%',
        paddingLeft: 20,
        paddingVertical: 8,
        marginVertical: 17,
    },
    crossIcon: {
        height: 20,
        width: 20,
        // marginTop: 4,
    },
    bottomContainerText: {
        color: Colors.redWhj,
        marginLeft: 12,
        paddingBottom: 4,
    },
    bottomNoCreditLossText: {
        color: Colors.redWhj,
        paddingBottom: 4,
    },
    pointContainerText: {
        color: Colors.greenWhj,
        marginLeft: 12,
        paddingBottom: 4,
    },
    bodyContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        textAlign: 'left',
        marginBottom: 16
    },
    activitiesContent: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginBottom: 16,
        borderBottomColor: Colors.greyPopupBorderColor,
        borderBottomWidth: 1,
        paddingBottom: 16,
    },
    activityTextStyle: {
        color: Colors.orangeCourseActivity,
        alignSelf: 'flex-start',
        marginLeft: 0,
    },
    styleCalendar: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
    },
    styleClock: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
    },
    styleTeacher: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
    },
    styleEmail: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
    },
    otpContainer: {
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
        marginHorizontal: 16,
        paddingTop: 16,
    },
    otpDetailsContainer: {
        alignSelf: 'flex-start',
        color: Colors.blackTextWhj,
        marginBottom: 7,
    },
    cancelClassInvalidStyle: {
        color: Colors.cancelClassTimerText,
        alignSelf: 'flex-start',
        marginTop: 16,
    },
    summaryBottomContainer: {
        height: 80,
        width: SCREEN_WIDTH,
        backgroundColor: Colors.whiteWhj,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iosSummaryBottomContainer: {
        height: 80,
        width: SCREEN_WIDTH,
        backgroundColor: Colors.whiteWhj,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
    },
    androidSummaryBottomContainer: {
        height: 80,
        width: SCREEN_WIDTH - 32,
        marginHorizontal: 16,
        backgroundColor: Colors.whiteWhj,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
    },
    innerBoxSummaryBottomContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: SCREEN_WIDTH - 32,
        backgroundColor: Colors.whiteWhj,
        marginBottom: 8,
    },
    upcomingBottomStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
        paddingTop: 8,
        paddingBottom: 8,
    },
    cancelledClassButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
    },
    scheduleNowStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
        paddingHorizontal: 16,
    },
    notCompletedCancelledClassButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
    },
    cancleClassOTPBottomMargin: (value) => ({
        marginBottom: value,
    }),
    selectTeacherCTA: {
        width: SCREEN_WIDTH - 32,
        borderWidth: 0.5,
        borderColor: Colors.greyDisabledWhj,
        //flex: 1,
    },
    rightMostButtonStyle: {
        marginVertical: 16,
        marginLeft: 16,
        paddingHorizontal: 16,
    },
    boosterScheduleStyle: {
        marginVertical: 16,
        marginLeft: 16,
        paddingHorizontal: 16,
    },
    leftMostButtonStyle: {
        marginVertical: 16,
    },
    cancelClassrightMostButtonStyle: {
        marginVertical: 16,
        marginLeft: 16,
        paddingHorizontal: 16,
        width: 165,
    },
    cancelClassContainerStyle: {
        color: Colors.greyDarkWhj,
    },
    cancelledClassPopupButton: {
        flex: 1,
        width: '100%',
    },
    notCompletedCancelledClassPopupButton: {
        flex: 1,
        width: SCREEN_WIDTH - 32,
    },
    notCompletedCenterAnimationNotFlex: {
        alignItems: 'center',
    },
    notCompletedCenterAnimation: {
        flex: 1,
        alignItems: 'center',
    },
    bodyContainer: {
        marginLeft: 20
    },
    teacherContainer: {
        marginLeft: 20,
        flex: 1,
    },
    bodyTeacherContainer: {
        flexDirection: 'row',
        width: '70%'
    },
    bodyTeacherContainerText: {
        width: '100%'
    },
    selectTeacherHeaderContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 16,
        marginHorizontal: 16,
    },
    defaultTeacherContainer: {
        marginTop: 2,
        borderRadius: 14,
        backgroundColor: Colors.greenLightWhj,
        paddingHorizontal: 7,
        marginLeft: 8,
    },
    defaultTeacherText: {
        color: '#4EA97C',
    },
    substituteTeacherContainer: {
        margin: 1,
        marginLeft: 8,
        borderRadius: 14,
        backgroundColor: Colors.blueLightWhj,
        height: 20
    },
    substituteTeacherText: {
        color: Colors.blueWhj,
        paddingHorizontal: 5,
    },
    substituteTeacherTitleStyle: {
        width: '100%'
    },
    tickImage: {
        width: 18,
        height: 18,
        marginRight: 2,
        resizeMode: 'cover',
        alignSelf: 'flex-end',
    },
    puncBadgeStyle: {
        width: 72,
        height: 74,
    },
    badgeImageStyle: {
        width: 72,
        height: 80,
        resizeMode: 'contain',
        overflow: 'hidden',
    },
    concentrationBadgeImageStyle: {
        width: 72,
        height: 80,
        resizeMode: 'contain',
        overflow: 'hidden',
        marginLeft: 6,
    },
    badgeTextStyle: {
        textAlign: 'center',
        color: Colors.blackTextWhj,
        marginTop: 2,
    },
    badgeContainer: {
        flexDirection: 'row',
    },
    badgeMargin: {
        marginRight: 29,
        marginBottom: 16,
    },
    descriptionMargin: {
        marginBottom: 16,
    },
    renewPopupContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    renewHeader: {
        color: Colors.blackTextWhj,
        marginVertical: 8,
    },
    renewButtonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
    },
    popUpHeading: {
        flex: 1,
        flexWrap: 'wrap',
    },
    classDescription: {
        color: Colors.blackTextWhj,
        marginHorizontal: 2,
        alignSelf: 'flex-start',
    },
    cancelledClassCreditLoss: {
        alignSelf: 'center',
        paddingTop: 2,
    },
    commonColorBlack: {
        color: Colors.blackWhj,
    },
    upcomingClassDefinitionStyle: {
        marginBottom: 16,
    },
    greyLineStyle: {
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
        marginBottom: 16,
    },
    classNameStyle: {
        color: Colors.blackTextWhj,
    },
    dayLimitDescriptionStyle: {
        alignSelf: 'center',
        textAlign: 'center',
        color: Colors.blackTextWhj,
    },
    radioContainer: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    selectedRadioButton: {
        flex: 1,
        justifyContent: 'flex-end',
        flexDirection: 'row',
    },
    radioButton: {
        marginRight: 3,
        justifyContent: 'flex-end',
        width: 15,
        borderRadius: 8,
        height: 15,
        borderWidth: 2,
        borderColor: '#E2E2EA',
    },
    listContainer: {
        flexDirection: 'row',
        marginBottom: 16,
        flex: 1
    },
    listItem: {
    },
    itemText: {
        color: Colors.blackTextWhj,
    },
    teacherEmail: {
        marginRight: 24,
    },
    losseCreditStyle: {
        flex: 1,
        flexWrap: 'wrap',
    },
    yourWillLosseCreditStyle: {
        flex: 1,
        flexWrap: 'wrap',
        marginTop: 8,
        alignSelf: 'flex-start',
    },
    lose1CreditTextStyle: {
        color: Colors.redWhj,
        alignSelf: 'flex-start',
        marginTop: 16,
    },
    classTypeContainer: {
        backgroundColor: Colors.blueLightWhj,
        zIndex: 99,
        marginBottom: 16,
    },
    classTypeSubContainer: { marginHorizontal: 16, marginVertical: 16 },
    classTypeHeadingText: { alignSelf: 'flex-start', color: Colors.greyDarkWhj },
    buttonStyle: {
        height: 48,
        backgroundColor: Colors.whiteWhj,
        justifyContent: 'center',
        borderRadius: 5,
        marginTop: 5,
    },
    buttonTextStyle: {
        alignSelf: 'flex-start',
        paddingLeft: 8,
        paddingVertical: 12,
        color: Colors.blackTextWhj,
    },
    noteText: { alignSelf: 'flex-start', color: Colors.greyDarkWhj },
    descTextStyle: { alignSelf: 'flex-start', color: Colors.blackAlternate },
    dropDownStyle: {
        backgroundColor: Colors.whiteWhj,
        borderRadius: 10,
        position: 'absolute',
        marginBottom: 16,
        alignSelf: 'center',
        justifyContent: 'center',
        zIndex: 99,
    },
    boosterDropDownContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: 22,
    },
    creditTextContainer: { marginHorizontal: 16, marginBottom: 16 },
    termAndConditionsItem: {
        flexDirection: 'row',
        marginBottom: 5,
        paddingRight: 10,
    },
    bulletDot: {
        height: 5,
        width: 5,
        borderRadius: 3,
        marginTop: 7,
        marginRight: 7,
        backgroundColor: Colors.blackShade,
    },
    termAndConditionsTitleText: {
        marginBottom: 10,
        color: Colors.blackShade,
    },
    termAndConditionsText: {
        alignSelf: 'flex-start',
        color: Colors.blackShade,
    },
    termAndConditionsFooter: {
        alignItems: 'center',
        justifyContent: 'center',
        borderTopColor: Colors.greyPopupBorderColor,
        borderTopWidth: 1,
        marginTop: 10,
        paddingTop: 8,
    },
});

export default Styles;
