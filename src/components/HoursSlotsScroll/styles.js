import {StyleSheet} from 'react-native';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  slotStyle: {
    marginHorizontal: 4,
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainerStyle: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingRight: 30,
  },
  selectedMathSlotButtonFontStyles: {
    color: Colors.whiteWhj,
  },
  selectedSlotButtonFontStyles: {
    color: '#ffffff',
  },
  newSlotFont: {
    color: Colors.greyDarkWhj,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ClassCompletedFontStyle: {
    color: Colors.blackTextWhj,
  },
  commonSlotStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    height: 40,
    width: 70,

    borderWidth: 0.35,
    borderColor: Colors.noSlotBorderColor,
    backgroundColor: Colors.whiteWhj,
  },
  ClassCompletedStyle: {
    backgroundColor: Colors.greyDisabledWhj,
    borderColor: Colors.greyDisabledWhj,
  },
  selectedMathSlotButtonStyles: {
    backgroundColor: Colors.blueWhj,
    borderColor: Colors.blueWhj,
    color: Colors.whiteWhj,
  },
  selectedSlotButtonStyles: {
    backgroundColor: Colors.orangeWhj,
    borderColor: Colors.orangeWhj,
    color: Colors.whiteWhj,
  },
  slotButtonStyles: {
    backgroundColor: Colors.whiteWhj,
    borderColor: Colors.noSlotBorderColor,
  },
  boosterButtonStyle: {
    backgroundColor: Colors.boosterColor,
    borderColor: Colors.blueWhj,
    color: Colors.whiteWhj,
  },
  slotColorStyle: {
    backgroundColor: Colors.grey6,
    color: Colors.lightGrey,
  },
});
