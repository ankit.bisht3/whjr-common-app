import Color from '../../common/Colors';
import { FontName, FontSizeBody } from '../../utility/FontsUtil';
import { GlobalStyles } from '../../common/GlobalStyles';

const OTPStyle = {
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 24,
    },
    androidBoxShadow: (index) => {
        return { flex: 1, margin: 4, marginLeft: index === 0 ? 0 : 4 };
    },
    textInput: {
        height: 50,
        width: 50,
        margin: 5,
        textAlign: 'center',
        fontSize: 22,
        fontFamily: FontName.whjDemi,
        color: Color.blackTextWhj,
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.whiteWhj,
        borderWidth: 0.5,
        borderColor: Color.blackTextWhj,
        height: 40,
        borderRadius: 5,
        margin: 10,
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 21,
        width: 16,
        resizeMode: 'stretch',
        alignItems: 'center',
    },
    containerStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: 50,
        flex: 1,
        marginRight: 4,
        borderColor: Color.greyPopupBorderColor,
        borderWidth: 0.5,
        borderRadius: 5,
        ...GlobalStyles.loginInputCardView,
    },
    invalidContainerStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: 50,
        flex: 1,
        marginRight: 4,
        borderColor: Color.redWhj,
        borderWidth: 0.5,
        borderRadius: 5,
        ...GlobalStyles.loginInputCardView,
    },
    leftImageOTPBoxStyle: {
        marginLeft: 0,
    },
    timerStyle: {
        margin: 8,
        alignItems: 'center',
        justifyContent: 'center',
        color: Color.orangeWhj,
        flex: 1,
    },
    timerTextStyle: {
        color: Color.redError,
        fontFamily: FontName.whjBook,
        fontSize: FontSizeBody.bodyThree,
    },
    resendOTPTextStyle: {
        color: Color.redError,
        fontFamily: FontName.whjBook,
        fontSize: FontSizeBody.bodyThree,
        height: 40,
        justifyContent: 'center',
    },
    cancelClassTimerTextStyle: {
        color: Color.cancelClassTimerText,
        fontFamily: FontName.whjDemi,
        fontSize: FontSizeBody.bodyTwo,
    },
    otherLoginStyle: {
        fontSize: FontSizeBody.bodyFour,
        fontFamily: FontName.whjDemi,
        color: Color.orangeWhj,
    },
    animatedTimerTextStyle: {
        position: 'absolute',
    },
};

export default OTPStyle;
