import React, { Component } from 'react';
import {
    View,
    TextInput,
    Text,
    TouchableOpacity,
    Platform,
} from 'react-native';
import { AuthScreenImages } from '../../common/Images';
import { LOGIN_SCREEN_STATES } from '../../constants/enum';
import ImageWhj from '../../presentation/Image';
import PropTypes from 'prop-types';
import OTPStyle from './styles';
import Localisation from '../../localisation';
import { androidOtpInputBoxShadow } from '../../common/GlobalStyles';
import { isIOS, isAutoFillSupported, isAndroid, secondsToMinutes } from '../../utility';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';

let interval = '';

const getOTPTextChucks = (inputCount, inputCellLength, text) => {
    let otpText =
        text.match(new RegExp('.{1,' + inputCellLength + '}', 'g')) || [];
    otpText = otpText.slice(0, inputCount);
    return otpText;
};

const OtpBoxShadow = (props) => {
    if (isIOS()) {
        return <View style={{ flex: 1 }}>{props.children}</View>;
    }
    return (
        <View style={OTPStyle.androidBoxShadow(props.index)}>
            {/* <BoxShadow setting={androidOtpInputBoxShadow}>{props.children}</BoxShadow> */}
        </View>
    );
};

class OTPTextView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            focusedInput: 0,
            timeLeft: props?.otpTime
                ? props.otpTime
                : LOGIN_SCREEN_STATES.RESENT_OTP_TIMER,
            otpText: getOTPTextChucks(
                props.inputCount,
                props.inputCellLength,
                props.defaultValue,
            ),
        };
        this.inputs = [];
    }

    startResendCountDown() {
        interval = setInterval(() => {
            if (this.state.timeLeft === 0) {
                this.props.onPressResendOTPEnable(true);
                clearInterval(interval);
            } else {
                this.setState({
                    timeLeft: this.state.timeLeft - 1,
                });
            }
        }, 1000);
    }

    componentDidMount() {
        this.onInputFocus(1);
        this.startResendCountDown();
    }

    componentWillUnmount() {
        clearInterval(interval);
    }

    componentDidUpdate(prevState, prevProps) {
        if (
            this.props?.listenerOtp &&
            prevProps?.listenerOtp !== this.props?.listenerOtp &&
            this.props?.listenerOtp.length === 4 &&
            this.state.otpText.length === 0
        ) {
            this.onTextChange(this.props?.listenerOtp, 0);
        }
        if (this.props.resendFlag === true) {
            this.props.resendOTPFlag(false);
            this.setState(
                {
                    timeLeft: this.props?.otpTime
                        ? this.props.otpTime
                        : LOGIN_SCREEN_STATES.RESENT_OTP_TIMER,
                },
                () => {
                    this.clear();
                    this.startResendCountDown();
                },
            );
        }
    }

    basicValidation = (text) => {
        const validText = /^[0-9]+$/;
        return text.match(validText);
    };

    onTextChange = (text, i) => {
        const { inputCellLength, inputCount, handleTextChange } = this.props;
        if (text && !this.basicValidation(text)) {
            return;
        }

        this.setState(
            (prevState) => {
                let { otpText } = prevState;
                if (
                    i === 0 &&
                    (isAutoFillSupported() || isAndroid()) &&
                    text.length === 4
                ) {
                    otpText[0] = text[0];
                    otpText[1] = text[1];
                    otpText[2] = text[2];
                    otpText[3] = text[3];
                } else if (
                    i === 0 &&
                    (isAutoFillSupported() || isAndroid()) &&
                    text.length > 1
                ) {
                    for (let j = 0; j < text.length; j++) {
                        otpText[j] = text[j];
                    }
                } else {
                    otpText[i] = text;
                }
                return {
                    otpText,
                };
            },
            () => {
                handleTextChange(this.state.otpText.join(''));
                if ((isAutoFillSupported() || isAndroid()) && text.length === 4) {
                    this.inputs[3].focus();
                } else if ((isAutoFillSupported() || isAndroid()) && text.length > 1) {
                    this.inputs[text.length].focus();
                } else if (text.length === 1 && i !== inputCount - 1) {
                    this.inputs[i + 1].focus();
                }
            },
        );
    };

    onInputFocus = (i) => {
        const { otpText } = this.state;
        const prevIndex = i - 1;
        if (prevIndex > -1 && !otpText[prevIndex] && !otpText.join('')) {
            this.inputs[prevIndex].focus();
            return;
        }
        this.setState({ focusedInput: i });
    };

    onKeyPress = (e, i) => {
        const val = this.state.otpText[i] || '';
        if (e.nativeEvent.key === 'Backspace' && i !== 0 && !(val.length - 1)) {
            this.inputs[i - 1].focus();
        }
    };

    clear = () => {
        this.setState(
            {
                otpText: [],
            },
            () => {
                this.inputs[0].focus();
            },
        );
    };

    setValue = (value) => {
        const { inputCount, inputCellLength } = this.props;
        this.setState(
            {
                otpText: getOTPTextChucks(inputCount, inputCellLength, value),
            },
            () => {
                this.props.handleTextChange(value);
            },
        );
    };

    render() {
        const {
            inputCount,
            offTintColor,
            tintColor,
            defaultValue,
            inputCellLength,
            containerStyle,
            textInputStyle,
            keyboardType,
            ...textInputProps
        } = this.props;

        const { focusedInput, otpText } = this.state;

        const TextInputs = [];
        if (!this.props?.isClassCancel) {
            TextInputs.push(
                <OtpBoxShadow index={0}>
                    <View
                        key="lock"
                        style={[OTPStyle.containerStyle, OTPStyle.leftImageOTPBoxStyle]}>
                        <ImageWhj imageData={AuthScreenImages.lockIcon} />
                    </View>
                </OtpBoxShadow>,
            );
        }
        for (let i = 0; i < inputCount; i += 1) {
            const inputStyle = [
                OTPStyle.textInput,
                textInputStyle,
                { borderColor: offTintColor },
            ];

            if (focusedInput === i) {
                inputStyle.push({ borderColor: tintColor });
            }

            TextInputs.push(
                <OtpBoxShadow>
                    <View
                        key={i}
                        style={
                            this.props.validatingOTP
                                ? OTPStyle.invalidContainerStyle
                                : OTPStyle.containerStyle
                        }>
                        <TextInput
                            ref={(e) => {
                                this.inputs[i] = e;
                            }}
                            key={i}
                            autoCorrect={false}
                            keyboardType={keyboardType}
                            autoFocus={Platform.OS === 'ios' ? true : false}
                            value={otpText[i] || ''}
                            style={inputStyle}
                            maxLength={
                                (isAutoFillSupported() || isAndroid()) && i === 0
                                    ? 4
                                    : this.props.inputCellLength
                            }
                            onFocus={() => this.onInputFocus(i)}
                            onChangeText={(text) => this.onTextChange(text, i)}
                            multiline={false}
                            textContentType={
                                isAutoFillSupported() || isAndroid() ? 'oneTimeCode' : 'none'
                            }
                            onKeyPress={(e) => this.onKeyPress(e, i)}
                            {...textInputProps}
                        />
                    </View>
                </OtpBoxShadow>,
            );
        }

        TextInputs.push(
            <View key="timer" style={OTPStyle.timerStyle}>
                {!this.props.isClassCancel && this.state.timeLeft > 0 ? (
                    <Text style={OTPStyle.timerTextStyle}>
                        {this.props.validatingOTP
                            ? Localisation.otpInvalid
                            : `${this.state.timeLeft} ${Localisation.otpTimerSec}`}
                    </Text>
                ) : null}
                {this.props.isClassCancel && this.state.timeLeft > 0 ? (
                    <Text style={OTPStyle.cancelClassTimerTextStyle}>
                        {secondsToMinutes(this.state.timeLeft)}
                    </Text>
                ) : (
                    this.props.isClassCancel && (
                        <TouchableOpacity
                            onPress={() => {
                                this.props.resendCancelClassOTP();
                            }}
                            style={OTPStyle.resendOTPTextStyle}>
                            <TextWhj
                                type={TextTypes.bodyThreeBookText}
                                title={Localisation.resendOTP}
                                style={OTPStyle.otherLoginStyle}
                            />
                        </TouchableOpacity>
                    )
                )}
            </View>,
        );

        return (
            <View style={[OTPStyle.container, containerStyle]}>{TextInputs}</View>
        );
    }
}

OTPTextView.propTypes = {
    defaultValue: PropTypes.string,
    inputCount: PropTypes.number,
    containerStyle: PropTypes.any,
    textInputStyle: PropTypes.any,
    inputCellLength: PropTypes.number,
    tintColor: PropTypes.string,
    offTintColor: PropTypes.string,
    handleTextChange: PropTypes.func,
    onPressResendOTPEnable: PropTypes.func,
    resendCancelClassOTP: PropTypes.func,
    inputType: PropTypes.string,
    keyboardType: PropTypes.string,
    validatingOTP: PropTypes.bool,
    isClassCancel: PropTypes.bool,
};

OTPTextView.defaultProps = {
    defaultValue: '',
    inputCount: 4,
    validatingOTP: false,
    isClassCancel: false,
    tintColor: '#3CB371',
    offTintColor: '#DCDCDC',
    inputCellLength: 1,
    containerStyle: {},
    textInputStyle: {},
    otpTimerStyle: {},
    handleTextChange: () => { },
    onPressResendOTPEnable: () => { },
    resendCancelClassOTP: () => { },
    keyboardType: 'numeric',
};

export default OTPTextView;
