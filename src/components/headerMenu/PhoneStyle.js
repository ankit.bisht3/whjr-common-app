import {StyleSheet} from 'react-native';
import Colors from '../../common/Colors';
import { FontName } from '../../utility/FontsUtil';
import { GlobalStyles } from '../../common/GlobalStyles';
import { isIOS } from '../../utility';
import { IS_BYJUS_APP } from '../../configuration/byjus';
const Styles = StyleSheet.create({
  logoContainer: {
    height: '100%',
    width: IS_BYJUS_APP ? 72 : 32,
    marginLeft: 16,
  },
  headerTitleStyles: {
    marginBottom: 10,
  },
  mainLogoStyle: {
    height: 32,
    width: '100%',
    resizeMode: 'contain',
  },
  backButtonStyle: {
    height: 28,
    width: 30,
  },
  logoStyle: {
    height: 24,
    width: 24,
  },
  phoneLogo: {
    height: 18,
    width: 18,
    marginTop: 3,
  },
  titleStyle: {
    marginHorizontal: 20,
    color: Colors.blackWhj,
    alignSelf: 'center',
  },
  titleStyletrail: {
    color: Colors.blackWhj,
    alignSelf: 'center',
    lineHeight: 20,
  },
  booktrail: {
    marginHorizontal: 20,
    color: Colors.orangeWhj,
  },
  menuTitleStyle: {
    color: Colors.blackTextWhj,
    alignSelf: 'flex-start',
  },
  moreLogoStyle: {
    padding: 16,
    marginLeft: 8,
  },
  backButtonContainerStyle: {
    marginLeft: 18,
  },
  notificationLogoStyle: {
    margin: 16,
    marginRight: 0,
  },
  headerRightStyle: {
    flexDirection: 'row',
  },
  container: {
    backgroundColor: Colors.whiteWhj,
    ...GlobalStyles.loginInputCardView,
  },
  tochableContainer: {
    flex: 1,
    alignItems: 'flex-end',
  },
  menuItemStyle: {
    paddingVertical: 16,
    marginHorizontal: 24,
    width: '100%',
  },
  menuContainer: {
    flexDirection: 'row',
    marginRight: 22,
    // borderWidth:1,
  },
  modalContainer: {
    alignItems: 'flex-start',
    backgroundColor: Colors.whiteWhj,
    paddingHorizontal: 8,
    paddingVertical: 8,
    borderRadius: 8,
    shadowOpacity: 0.75,
    elevation: 10,
    shadowRadius: 5,
    shadowColor: Colors.borderColorWhj,
    shadowOffset: {height: 0, width: 0},
  },
  modalContent: {
    backgroundColor: Colors.whiteWhj,
    padding: 16,
    borderWidth: 1,
  },
  customHeaderTitleStyle: {
    textAlign: 'center',
    alignSelf: 'center',
  },
  customHeaderStyle: {
    shadowRadius: 10,
    shadowOpacity: 0.025,
    shadowOffset: {
      width: 0,
      height: 0.5,
    },
    shadowColor: Colors.noSlotBorderColor,
    borderBottomColor: isIOS()
      ? Colors.headeriOSBottomBorderColor
      : Colors.headerAndroidBottomBorderColor,
    borderBottomWidth: 1,
    elevation: 1,
  },
  arrowView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  leftArrow: {
    height: 24,
    width: 24,
    alignSelf: 'center',
    transform: [{rotate: '180deg'}],
  },
  iconStyle: {
    height: 24,
    width: 24,
    alignSelf: 'center',
  },
  title: {
    color: Colors.orangeWhj,
    alignSelf: 'center',
    marginHorizontal: 24,
  },
  notificationIconContainer: {
    position: 'absolute',
    height: 22,
    width: 22,
    top: -8,
    right: -8,
    borderRadius: 22 / 2,
    borderWidth: 1,
    borderColor: Colors.redWhj,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.redWhj,
  },
  notificationIconBadge: {
    fontFamily: FontName.whjDemi,
    fontSize: 10,
    fontWeight: '700',
    color: Colors.whiteWhj,
  },
});

export default Styles;
