import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  StatusBar,
  Linking,
} from 'react-native';
import {
  COURSE_TYPE_STATUS,
  HEADER_MENU_ITEMS,
  MENU_ITEM_CONSTANT,
  USER_CATEGORY,
  USER_TYPE,
  MATH_HEADER_MENU_ITEMS,
  USER_MATH_CATEGORY,
  MUSIC_HEADER_MENU_ITEMS,
} from '../../constants/enum';
import TextWhj from '../../presentation/Text';
import TextTypes from '../../presentation/Text/TextTypes';
// import {useHeaderHeight} from '@react-navigation/stack';
import config from '../../configuration';
import Styles from './PhoneStyle';
// import ScreenNames from '../../constants/Screens';
// import {recordEvent} from '../../manager/analytics';
// import {EVENT_TYPE} from '../../manager/analytics/AnalyticsEventType';
// import {clearAllData} from '../../manager/storage';
// import {removeHatsOffShownStatus} from '../../utility';
import Localistion from '../../localisation';

export const handleCb = (props, url, titleName = '', eventName) => {
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      props.navigation.navigate(ScreenNames.DashboardWebScreen, {
        url,
        webScreenTitle: titleName,
      });
      props.navigation.setParams({
        isMenuModal: false,
      });
      // recordEvent(eventName);
    } else {
      //  console.log("Don't know how to open URI: " + url);
    }
  });
};

const HeaderMenu = (props) => {
  const statusBarHeight = StatusBar?.currentHeight;
  const headerHeight = 50; //useHeaderHeight();
  const userType =
    props.currentScreen === COURSE_TYPE_STATUS.MATH
      ? props?.userMathCategory === USER_MATH_CATEGORY.USER_PAID
        ? USER_TYPE.PAID
        : USER_TYPE.TRIAL
      : props?.userCategory === USER_CATEGORY.USER_PAID
      ? USER_TYPE.PAID
      : USER_TYPE.TRIAL;

  const userMathType =
    props?.userMathCategory === USER_CATEGORY.USER_PAID
      ? USER_TYPE.PAID
      : USER_TYPE.TRIAL;

  const isIndianUser = props?.userInfo?.countryCode === 'IN';

  const menuItemClicked = (menuItem) => {
    const {userInfo} = props;
    // recordEvent(menuItem?.analyticsKey, {studentId: userInfo?.userStudentId});
    // if (menuItem.key === MENU_ITEM_CONSTANT.CAPSTONE) {
    //   recordEvent(EVENT_TYPE.capstone.PAGEVIEW_CAPSTONE, {
    //     course_type: props?.selectedCourseType,
    //     studentId: userInfo?.userStudentId,
    //   });
    // }
    if (menuItem.key === MENU_ITEM_CONSTANT.USER_LOGOUT) {
      clearAllData();
      removeHatsOffShownStatus(props.sessionId); // Clearing hatsOff data from Async
      props.logout();
    } else if (menuItem.key === MENU_ITEM_CONSTANT.MY_QUIZEES) {
      props.navigation.navigate(ScreenNames.quiz);
      props.navigation.setParams({
        isMenuModal: false,
      });
    } else if (menuItem.key === MENU_ITEM_CONSTANT.MY_REPORT_CARD) {
      props.navigation.navigate(ScreenNames.StudentReport);
      props.navigation.setParams({
        isMenuModal: false,
      });
    } else if (menuItem.key === MENU_ITEM_CONSTANT.PERSONAL_CONCIERGE) {
      props.navigation.navigate(ScreenNames.personalConcierge);
      props.navigation.setParams({
        isMenuModal: false,
      });
    } else if (menuItem.key === MENU_ITEM_CONSTANT.CERTIFICATES) {
      props.navigation.setParams({
        isMenuModal: false,
      });
      props.navigation.navigate(ScreenNames.studentCertificates);
    } else {
      handleCb(
        props,
        menuItem.navigationUrl(
          config?.apis?.webBaseUrl,
          props?.token,
          props?.studentInfo?.portfolioUrl,
        ),
        menuItem?.name,
      );
    }
  };
  let MENU_ITEMS;
  props.currentScreen === COURSE_TYPE_STATUS.MATH
    ? (MENU_ITEMS = MATH_HEADER_MENU_ITEMS)
    : props.currentScreen === COURSE_TYPE_STATUS.MUSIC
    ? (MENU_ITEMS = MUSIC_HEADER_MENU_ITEMS)
    : (MENU_ITEMS = HEADER_MENU_ITEMS);
  return (
    <>
      <SafeAreaView style={Styles.container}>
        <Modal
          animationType={props.animationType}
          transparent={props.transparent}
          style={Styles.modalContent}
          onRequestClose={() => props.setActionModalVisible()}>
          <TouchableOpacity
            style={Styles.tochableContainer}
            onPress={() => props.setActionModalVisible()}>
            <View style={[Styles.menuContainer, {marginTop: headerHeight - 8}]}>
              <View style={Styles.modalContainer}>
                {MENU_ITEMS.filter((item) => {
                  if (
                    item.key === MENU_ITEM_CONSTANT.MANAGE_MY_WEBSITE &&
                    props.userCourseType !== COURSE_TYPE_STATUS.MATH &&
                    isIndianUser
                  ) {
                    return true;
                  } else if (
                    item.key === MENU_ITEM_CONSTANT.CERTIFICATES &&
                    !props?.studentInfo?.trialCertificateLink &&
                    props?.studentInfo?.certificates &&
                    Object.values(props?.studentInfo?.certificates).length === 0
                  ) {
                    return false;
                  } else {
                    return props.userCourseType === COURSE_TYPE_STATUS.MATH
                      ? item.userType.indexOf(userMathType) > -1
                      : item.userType.indexOf(userType) > -1;
                  }
                }).map((menuItem) => (
                  <TouchableOpacity
                    key={menuItem.key}
                    // onPress={() => menuItemClicked(menuItem)}
                    style={Styles.menuItemStyle}>
                    <TextWhj
                      title={
                        Localistion.screenNames[menuItem.name]
                          ? Localistion.screenNames[menuItem.name]
                          : Localistion[menuItem.name] || menuItem.name
                      }
                      type={TextTypes.bodyThreeDemiText}
                      style={Styles.menuTitleStyle}
                    />
                  </TouchableOpacity>
                ))}
              </View>
            </View>
          </TouchableOpacity>
        </Modal>
      </SafeAreaView>
    </>
  );
};

HeaderMenu.propTypes = {
  height: PropTypes.number,
  transparent: PropTypes.bool,
  animationType: PropTypes.string,
  scrollEnabled: PropTypes.bool,
  style: PropTypes.object,
  setActionModalVisible: PropTypes.func,
  onClickMenuItem: PropTypes.func,
  logout: PropTypes.func,
};

HeaderMenu.defaultProps = {
  style: {},
  transparent: true,
  animationType: 'fade',
  scrollEnabled: false,
  userCategory: null,
  setActionModalVisible: () => {},
  onClickMenuItem: () => {},
  logout: () => {},
};
export default HeaderMenu;
// const mapStateToProps = ({userInfo}) => {
//   return {
//     userInfo: userInfo?.userInfo,
//     token: userInfo?.token,
//     selectedCourseType: userInfo?.selectedCourseType,
//     userCategory: userInfo?.userInfo?.userCategory,
//     userMathCategory: userInfo?.userInfo?.userMathCategory,
//     studentInfo: userInfo?.studentInfo,
//     userCourseType: userInfo?.userInfo?.courseType,
//     currentScreen: userInfo?.selectedCourseType,
//   };
// };

// export default connect(mapStateToProps, {
//   logout,
// })(HeaderMenu);
