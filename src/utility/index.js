import { COURSE_TYPE_STATUS, DATE_FORMAT_ENUM } from '../constants/enum';
import moment from 'moment-timezone';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { COMMON_AXIOS_QUERIES, USER_INFO, GLOBAL_CONFIG } from '../common/AsyncKeys';

export const isAndroid = () => {
    return Platform.OS === 'android';
};

export const isIOS = () => {
    return Platform.OS === 'ios';
};

export const camelCase = (str) => {
    if (str?.length) {
        var splitStr = trimString?.(str)?.toLowerCase?.()?.split?.(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] =
                splitStr?.[i]?.charAt?.(0)?.toUpperCase() + splitStr?.[i]?.substring?.(1);
        }
        return splitStr?.join?.(' ');
    }
    return '';
};

export const isAutoFillSupported = () => {
    const majorVersionIOS = parseInt(String(Platform.Version), 10);
    return isIOS() && majorVersionIOS >= 12;
};

export const secondsToMinutes = (seconds = 60) => {
    const min = Math.floor(seconds / 60);
    const sec = Math.floor(seconds % 60);
    return (min < 10 ? '0' : '') + min + ':' + ((sec < 10 ? '0' : '') + sec);
};

export const trimString = (str) => {
    if (str?.length) {
        return str?.replace?.(/^\s+|\s+$/g, '');
    }
    return '';
};

export const dateFormatter = (dateValue, format, timeZone) => {
    if (timeZone) {
        return moment(dateValue).tz(timeZone).format(format);
    } else {
        // const state = store?.getState();
        // const tz = state?.userInfo?.userInfo?.timezone;
        // return moment(dateValue).tz(tz).format(format);
        return moment(dateValue).format(format);
    }
};

export const getClassTime = (startTime, endTime) => {
    return `${dateFormatter(
        startTime,
        DATE_FORMAT_ENUM.TIME_FORMAT_AMPM,
    )} - ${dateFormatter(endTime, DATE_FORMAT_ENUM.TIME_FORMAT_AMPM)}`;
};

export const getDefaultTeacherObject = (teacherList, defaultTeacherId) => {
    let defaultTeacher = teacherList?.filter(function (item) {
        return item.id === defaultTeacherId;
    });
    return defaultTeacher ? defaultTeacher?.[0] : {};
};

export const isCourseTypeCoding = (courseType) => {
    if (!courseType) {
        return null;
    }
    return courseType === COURSE_TYPE_STATUS.CODING ? true : false;
};

export const needToShowReviewPopup = () => {
    // const state = store?.getState();
    const startDate = "" //state?.userInfo?.reviewPopUpStartDate;
    const monthDifference = startDate && moment().diff(startDate, 'months', true);
    return monthDifference >= 1;
};

export const isCourseTypeMath = (courseType) => {
    if (!courseType) {
        return null;
    }
    return courseType === COURSE_TYPE_STATUS.MATH ? true : false;
};

/**
 * Method to add courseType param at api client level
 * @param {*} courseType
 * @param {*} selectedCourseType
 * @returns
 */
export const checkCourseType = (courseType, selectedCourseType) => {
    // At the time of 3-field registeration if we don't know courseType we have added courseType as ALL for backward compatibility
    if (!courseType) {
        return 'ALL';
    }
    return selectedCourseType;
};

export const isRunningFromOtherRepo = async () => {
    const commonQueries = await AsyncStorage.getItem(COMMON_AXIOS_QUERIES);
    return !!commonQueries; // returning bool
}

export const getAxiosCommonParams = async () => {
    const asynCommonHeaders = await AsyncStorage.getItem(COMMON_AXIOS_QUERIES);
    return asynCommonHeaders ? JSON.parse?.(asynCommonHeaders) : null
}

export const saveUserInfo = async (data) => {
    await AsyncStorage.setItem(USER_INFO, JSON.stringify(data))
}

export const getUserInfo = async () => {
    const userInfo = await AsyncStorage.getItem(USER_INFO);
    return userInfo ? JSON.parse?.(userInfo) : null;
}

export const saveUserGlobalConfig = async (data) => {
    await AsyncStorage.setItem(GLOBAL_CONFIG, JSON.stringify(data))
}

export const getUserGlobalConfig = async () => {
    const globalConfig = await AsyncStorage.getItem(GLOBAL_CONFIG);
    return globalConfig ? JSON.parse?.(globalConfig) : null;
}