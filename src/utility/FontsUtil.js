export const FontName = {
    whjBold: 'FuturaPT-Bold',
    whjDemi: 'FuturaPT-Demi',
    whjBook: 'FuturaPT-Book',
    whjMedium: 'FuturaPT-Medium',
};

export const FontSizeHeading = {
    headingOne: 20,
    headingTwo: 18,
    headingThree: 16,
    headingFour: 14,
    headingFive: 12,
    headingSix: 10,
    headingLarge: 24,
    headingLarger: 32,
    headingExtraLarge: 40,
    selectYourClassText: 23,
};

export const FontSizeBody = {
    bodyOne: 18,
    bodyTwo: 16,
    bodyThree: 14,
    bodyFour: 12,
    bodyFive: 10,
    bodySix: 32,
};

export const FontSizeButton = {
    buttonExtraLarge: 18,
    buttonLarge: 16,
    buttonMedium: 14,
    buttonSmall: 14,
    buttonExtraSmall: 12,
};
