import { getAvaiableTeacherList, getOneTimeScheduler } from "../apiClient/ApiCalls";
import { scheduleSlotParsingModel } from "../apiClient/model/ScheduleSlotParsingModel";
import { teacherModel } from "../apiClient/model/TeacherModel";
import { getUserInfo } from "../utility";
import { TIME_ZONE } from "../constants/enum";

export const getOneTimeSchedulerSlots = async (studentId, teacherId) => {
    try {
        const response = await getOneTimeScheduler(studentId, {
            teacherId: teacherId,
        });
        const userInfo = await getUserInfo();
        const timezone = userInfo?.timezone || TIME_ZONE?.ASIA;
        console.log("timezonetimezone", timezone)
        let payload;
        if (response && response?.data && response?.data?.data) {
            payload = scheduleSlotParsingModel(response?.data?.data, timezone);
        }
        return payload;
    } catch (error) {
    }
};

export const getSubstituteTeacherForOneTimeScheduler = async (studentId) => {
    try {
        const teacherList = await getAvaiableTeacherList(studentId);
        const payload = teacherList.data?.data?.map((teacher) => {
            return teacherModel(teacher);
        });
        return payload
    } catch (error) {
    }
};