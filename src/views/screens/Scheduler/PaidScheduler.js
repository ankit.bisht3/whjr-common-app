import React, { Component } from 'react';
import {
  Image,
  View,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  SectionList,
} from 'react-native';

import PhoneStyles from './styles';
import TextWhj from '../../../presentation/Text';
import TextTypes from '../../../presentation/Text/TextTypes';
import Localisation from '../../../localisation';
import { GlobalStyles } from '../../../common/GlobalStyles';
import { ScheduleScreenImage, ptmClassIcon } from '../../../common/Images';
import ScheduleClassPopup from '../../../components/classPopup/ScheduleClassPopup';
import NoCreditLeftPopup from '../../../components/classPopup/NoCreditLeftPopup';
import Covid19LockdownPopup from '../../../components/classPopup/Covid19LockdownPopup';
import ClassScheduledConfirmedPopup from '../../../components/classPopup/ClassScheduledConfirmedPopup';
import SlotCancelledConfirmation from '../../../components/classPopup/SlotCancelledConfirmation';
import DayLimitPopup from '../../../components/classPopup/DayLimitPopup';
import UpcomingClassDetail from '../../../components/classPopup/UpcomingClassDetail';
import HeaderMenu from '../../../components/headerMenu/HeaderMenu';
// import SelectTeacher from '../../components/classPopup/SelectTeacher';
// import Covid19LockdownPopup from '../../components/classPopup/Covid19LockdownPopup';
// import {connect} from 'react-redux';
// import {
//   getSubstituteTeacherForOneTimeScheduler,
//   getOneTimeSchedulerSlots,
//   scheduleOnTimeSlot,
//   classesBookedViaDevice,
// } from '../../../redux/actions/ScheduleClassAction';
// import {cancelUpcomingStudentClass} from '../../../redux/actions/ClassesAction';

import { DATE_FORMAT_ENUM, CLASS_TYPE, COURSE_TYPE_STATUS, CLASS_MODAL_TYPES, WEB_URLS, SCHEDULE_WEEK_TITLE_ENUM } from '../../../constants/enum';
import { WEEKS, IOS_PLATFORM, MAX_SLOT_BOOKING_LIMIT_DAY, DUMMY_STUDENT_ID, DUMMY_TEACHER_ID } from '../../../constants/GeneralConstants';
// import NoSlotView from '../../common/noSlotView';
import {
  dateFormatter,
  getClassTime,
  getDefaultTeacherObject,
  isIOS,
  isCourseTypeCoding,
  needToShowReviewPopup,
  isRunningFromOtherRepo,
  getUserGlobalConfig
} from '../../../utility';

// import ClassScheduledConfirmedPopup from '../../components/classPopup/ClassScheduledConfirmedPopup';
// import SlotCancelledConfirmation from '../../components/classPopup/SlotCancelledConfirmation';
// import Loader from '../../../presentation/loader';
// import HeaderMenu from '../../../presentation/screenHeader/HeaderMenu';
// import NoCreditLeftPopup from '../../components/classPopup/NoCreditLeftPopup';
// import ScreenNames from '../../../constants/Screens';
import Configuration from '../../../configuration';
// import {
//   reviewPopUpShowAction,
//   updateReloadStatus,
// } from '../../../redux/actions/GlobalAction';
// import { recordEvent } from '../../../manager/analytics';
// import { EVENT_TYPE } from '../../../manager/analytics/AnalyticsEventType';
import Colors from '../../../common/Colors';
import { LIMIT_REACHED, CLASS_BOOKED_VIA_THIS_DEVICE, PROJECT_BOOSTER_EVENT, SCREEN_TYPE_BOOSTER_CLASS } from '../../../constants/enum';
// import DayLimitPopup from '../../components/classPopup/DayLimitPopup';
// import UpcomingClassDetail from '../../components/classPopup/UpcomingClassDetail';
import InAppReview from 'react-native-in-app-review';
// import { HoursSlotsScroll } from '../../components/Schedule';
import { SCREEN_WIDTH } from '../../../utility/SizeUtil';
// import { getBoosterClassBook } from '../../../redux/actions/BoosterSubmitAction';
import { IS_BYJUS_APP } from '../../../configuration/byjus';
import { getOneTimeSchedulerSlots, getSubstituteTeacherForOneTimeScheduler } from '../../../actions/SchedulerActions';
import SelectTeacher from '../../../components/classPopup/SelectTeacher';
import HoursSlotsScroll from '../../../components/HoursSlotsScroll';
// import { getBoosterEligibleToBook } from '../../../redux/actions/BoosterProjectAction';
import Loader from '../../../components/loader';

const WEEK_HEADER_DATA = IS_BYJUS_APP ? WEEKS : WEEKS.slice(0, -2);
const WEEK_HEADER_CARD_WIDTH = 85;
class PaidScheduler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTeacher: {},
      selectedWeek: SCHEDULE_WEEK_TITLE_ENUM.WEEK_1,
      showScheduleClassPopup: false,
      selectedSlot: {},
      selectedSlotData: {},
      isVisible: CLASS_MODAL_TYPES.NO_MODAL,
      weekWiseSlotData: [],
      teacherList: [],
      isLoading: true,
      userConfig: null
    };
    this.ITEM_HEIGHT = 0;
    this.arr = [];
    this.teacherId = this.props.data?.teacherId || DUMMY_TEACHER_ID;
    this.studentId = this.props.data?.studentId || DUMMY_STUDENT_ID;
    this.selectedCourseType = this.props.data?.selectedCourseType || COURSE_TYPE_STATUS.CODING;
  }
  _selectWeek(week) {
    this.setState({
      selectedWeek: week,
    });
  }

  async componentDidMount() {
    // const { studentId } = this.props;
    // recordEvent(EVENT_TYPE.classes.PAGEVIEW_SCHEDULE_CLASS, {
    //   studentId: studentId,
    //   course_type: this.selectedCourseType,
    //   time_spent: '',
    // });
    this.setState({
      userConfig: await getUserGlobalConfig()
    })
    // If teacher id is present in userInfo. Api's called parallely else sequentially
    const defaultCourseTeacher = this.teacherId;
    if (defaultCourseTeacher) {
      getOneTimeSchedulerSlots(this.studentId, defaultCourseTeacher)
        .then((weekWiseData) => {
          console.log("weekWiseDataweekWiseData", weekWiseData)
          this.setState({ weekWiseSlotData: weekWiseData?.weekWiseSlotData });
        })
        .finally(() => {
          this.setState({ isLoading: false })
        });

      getSubstituteTeacherForOneTimeScheduler(this.studentId)
        .then((res) => {
          let defaultTeacher;
          if (defaultCourseTeacher) {
            defaultTeacher = getDefaultTeacherObject(
              res,
              defaultCourseTeacher,
            );
          } else {
            defaultTeacher = res?.[0];
          }
          this.setState({ selectedTeacher: defaultTeacher, teacherList: [defaultTeacher] });
          this.saveTeacherList(res)
        });
    } else {
      getSubstituteTeacherForOneTimeScheduler(this.studentId)
        .then((res) => {
          this.saveTeacherList(res)
          let defaultTeacher;
          if (defaultCourseTeacher) {
            defaultTeacher = getDefaultTeacherObject(
              res,
              defaultCourseTeacher,
            );
          } else {
            defaultTeacher = res?.[0];
          }
          this.setState({ selectedTeacher: defaultTeacher });
          getOneTimeSchedulerSlots(this.studentId, defaultTeacher?.id)
            .then((weekWiseData) => {
              this.setState({ weekWiseSlotData: weekWiseData?.weekWiseSlotData });
            })
            .finally(() => {
              this.setState({ isLoading: false })
            });
        })
        .catch(() => {
          this.setState({ isLoading: false })
        });
    }
  }

  saveTeacherList = (data) => {
    const payload = {
      GET_TEACHER_LIST_REQUEST: data
    }
    this.sendApiCallback(payload)
  }

  sendApiCallback = async (data) => {
    const _isRunningFromOtherRepo = await isRunningFromOtherRepo();
    _isRunningFromOtherRepo && this.props.apiCallback(data)
  }

  currentYPosition(index) {
    let yPosition = 0;
    if (this.arr?.length > 0 && this.arr?.length > index) {
      yPosition = this.arr?.slice(0, index + 1)?.reduce((a, b) => a + b, 0);
    }
    return yPosition;
  }

  handleScroll = ({ nativeEvent }) => {
    const offset = nativeEvent.contentOffset.y;
    let newIndex = 0;

    if (offset <= this.currentYPosition(0)) {
      newIndex = 0;
    } else if (offset <= this.currentYPosition(1)) {
      newIndex = 1;
    } else if (offset <= this.currentYPosition(2)) {
      newIndex = 2;
    } else if (offset <= this.currentYPosition(3)) {
      newIndex = 3;
    } else if (offset <= this.currentYPosition(4)) {
      newIndex = 4;
    } else if (offset <= this.currentYPosition(5)) {
      newIndex = 5;
    }

    if (this.state.selectedWeek !== WEEK_HEADER_DATA[newIndex]) {
      this.setState({ selectedWeek: WEEK_HEADER_DATA[newIndex] });
      const width = newIndex * WEEK_HEADER_CARD_WIDTH + 4;
      if (SCREEN_WIDTH - WEEK_HEADER_CARD_WIDTH < width) {
        this.horizontalScrolltRef.scrollTo({ x: width });
      } else {
        this.horizontalScrolltRef.scrollTo({ x: 0 });
      }
    }
  };

  /**
   * Function to handle click on week Tab
   * @param {*} week
   * @param {*} index
   */
  onClickWeekHeader(week, index) {
    if (
      !this.state?.weekWiseSlotData ||
      this.state?.weekWiseSlotData?.length <= 0
    ) {
      return;
    }
    this.sectionListRef?.scrollToLocation({
      sectionIndex: -1,
      itemIndex: index + 1,
      animated: true,
    });
    //By using this scroll immediately to position and the later updates the current week
    setTimeout(() => {
      this.setState({
        selectedWeek: week,
      });
    }, 200);
  }

  renderWeekElement() {
    const elems = WEEK_HEADER_DATA.map((week, index) => {
      return (
        <View
          key={index}
          style={{
            paddingHorizontal: 16,
            margin: 2,
            width: IS_BYJUS_APP
              ? WEEK_HEADER_CARD_WIDTH
              : (SCREEN_WIDTH - 36) / 4,
          }}>
          <TouchableOpacity
            style={PhoneStyles.weekHeaderTouch}
            onPress={() => this.onClickWeekHeader(week, index)}>
            <TextWhj
              style={
                this.state.selectedWeek === week
                  ? isCourseTypeCoding(this.props?.courseType)
                    ? PhoneStyles.selectedWeekFontStyles
                    : PhoneStyles.weekTextMath
                  : PhoneStyles.weekFontStyles
              }
              type={TextTypes.bodyTwoMediumText}
              title={week}
            />
          </TouchableOpacity>
        </View>
      );
    });
    return (
      <ScrollView
        showsHorizontalScrollIndicator={false}
        ref={(ref) => (this.horizontalScrolltRef = ref)}
        horizontal={true}
        style={PhoneStyles.weekContainerStyles}>
        {elems}
      </ScrollView>
    );
  }

  _getSelectedWeekDetails() {
    const data = this.state.weekWiseSlotData;

    for (let i = 0; i < data.length; i++) {
      if (data[i].week === this.state.selectedWeek) {
        return data[i];
      }
    }
  }

  renderWeekHeader(weekDetails) {
    const startDate = dateFormatter(
      weekDetails.days[0].day,
      DATE_FORMAT_ENUM.DD_MMM_YYYY,
    );
    const endDate = dateFormatter(
      weekDetails.days[weekDetails.days.length - 1].day,
      DATE_FORMAT_ENUM.DD_MMM_YYYY,
    );
    return (
      <View style={PhoneStyles.weekHeaderStyles}>
        <TextWhj
          style={PhoneStyles.weekText}
          type={TextTypes.bodyFourBookText}
          title={`${startDate} - ${endDate}`}
        />
      </View>
    );
  }

  isMaxLimitReached(weekDetails, daySlots) {
    let limitReached = '';
    let noOfSelectedSlotsWeek = 0;
    let noOfSelectedSlotsDay = 0;

    // Check for dayLimit
    for (let i = 0; i < daySlots.length; i++) {
      if (
        daySlots[i].booking &&
        daySlots[i].booking.id
        // && !daySlots[i].booking.isTrial
      ) {
        noOfSelectedSlotsDay++;
        if (
          noOfSelectedSlotsDay >= MAX_SLOT_BOOKING_LIMIT_DAY &&
          daySlots[i].booking?.courseType === this.props.courseType
        ) {
          limitReached = LIMIT_REACHED.DAY;
          break;
        }
      }
    }
    if (limitReached !== '') {
      return limitReached;
    }
    // Check for weekLimit
    weekDetails.days.forEach((day) => {
      const slots = day.slots || [];
      for (let i = 0; i < slots.length; i++) {
        if (
          slots[i].booking &&
          slots[i].booking.id &&
          !slots[i].booking.isTrial &&
          slots[i].booking?.courseType === this.props.courseType
        ) {
          noOfSelectedSlotsWeek++;
          if (noOfSelectedSlotsWeek >= this.maxClassesAllowedInWeek()) {
            limitReached = LIMIT_REACHED.WEEK;
            return;
          }
        }
      }
    });
    return limitReached;
  }

  _handleSlotClick = (slot, weekDetails, daySlots) => {
    console.log('_handleSlotClick', 'Slot clicked');
    const selectedSlotData = {
      classDate: dateFormatter(slot?.startTime, DATE_FORMAT_ENUM.FORMATE_1),
      classTime: getClassTime(slot?.startTime, slot?.endTime),
      classTeacher: slot?.teacher?.name,
      teacherId: slot?.teacher?.id,
      startTime: slot?.startTime,
      classTeacherEmail: slot?.teacher?.email,
      isBooster: slot?.booking?.courseClassType,
      courseType: slot?.booking?.courseType
    };
    console.log('_handleSlotClick', 'selectedSlotData : ', selectedSlotData);
    if (slot.booking && slot.booking.id) {
      console.log('_handleSlotClick', 'CLASS_MODAL_TYPES.CANCEL_SLOT_CLASS ');
      this.setState({
        isVisible: CLASS_MODAL_TYPES.CANCEL_SLOT_CLASS,
        selectedSlot: slot,
        selectedSlotData,
      });
    } else {
      if (this.props.credits > 0) {
        switch (this.isMaxLimitReached(weekDetails, daySlots)) {
          case LIMIT_REACHED.DAY:
            console.log('_handleSlotClick', 'CLASS_MODAL_TYPES.DAY_LIMIT_REACHED ');
            this.setState({
              isVisible: CLASS_MODAL_TYPES.DAY_LIMIT_REACHED,
              selectedSlot: null,
            });
            break;
          case LIMIT_REACHED.WEEK:
            // recordEvent(
            //   EVENT_TYPE.classes
            //     .TRIED_SCHEDULING_MORE_THAN_3_CLASSES_INAWEEK_POPUP,
            // );
            console.log('_handleSlotClick', 'CLASS_MODAL_TYPES.COVID_19_THRESHOLD ');
            this.setState({
              isVisible: CLASS_MODAL_TYPES.COVID_19_THRESHOLD,
              selectedSlot: null,
            });
            break;
          default:
            console.log('_handleSlotClick', 'CLASS_MODAL_TYPES.SCHEDULE_CLASS ');
            this.setState({
              isVisible: CLASS_MODAL_TYPES.SCHEDULE_CLASS,
              selectedSlot: slot,
              selectedSlotData,
            });
            break;
        }
      } else {
        console.log('_handleSlotClick', 'CLASS_MODAL_TYPES.NO_CREDIT_LEFT ');
        this.setState({
          isVisible: CLASS_MODAL_TYPES.NO_CREDIT_LEFT,
        });
        // this.setState({
        //   isVisible: CLASS_MODAL_TYPES.TEACHER_SUBSTITUTE,
        //   selectedSlot: slot,
        //   selectedSlotData,
        // });
      }
    }
  };

  _bookUpcomingSlot() {
    const { selectedSlot } = this.state;
    const {
      totalClassesCount,
      reviewPopUpStartDate
    } = this.props;
    this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
    let loaderTimer = null;
    if (Platform.OS === IOS_PLATFORM) {
      loaderTimer = setTimeout(() => {
        this.setState({ isLoading: true })
      }, 1000);
    } else {
      this.setState({ isLoading: true })
    }
    const isValid = !isIOS() && InAppReview.isAvailable();
    this.props
      .scheduleOnTimeSlot(selectedSlot, this.props?.courseItemId)
      .then((weekWiseData) => {
        if (loaderTimer) {
          clearTimeout(loaderTimer);
        }
        this.setState({ isLoading: false })
        if (weekWiseData) {
          this.setState({ weekWiseSlotData: weekWiseData?.weekWiseSlotData });
          this.props
            .classesBookedViaDevice(totalClassesCount + 1)
            .then((res) => {
              if (
                res ===
                CLASS_BOOKED_VIA_THIS_DEVICE.CLASS_BOOKED_VIA_THIS_DEVICE &&
                isValid
              ) {
                // recordEvent(EVENT_TYPE.inAppReview.PLAYSTORE_REFERRAL_POPUP);
                if (reviewPopUpStartDate) {
                  if (needToShowReviewPopup()) {
                    setTimeout(() => InAppReview.RequestInAppReview(), 45000);
                  }
                } else {
                  setTimeout(() => InAppReview.RequestInAppReview(), 45000);
                  this.props.reviewPopUpShowAction();
                }
              }
            });
          this.setState({ isLoading: false })
          this.props.updateReloadStatus(true, true);
          this.setState({ isVisible: CLASS_MODAL_TYPES.SLOT_CONFIRMED });
          // recordEvent(EVENT_TYPE.classes.PAIDCLASS_SCHEDULED_SUCCESS, {
          //   start_time: selectedSlot?.startTime,
          //   studentId: studentId,
          //   course_type: this.selectedCourseType,
          //   sequence_no: selectedSlot?.sequenceNo,
          //   classType: PROJECT_BOOSTER_EVENT.NORMAL,
          // });
        }
      })
      .catch(() => {
        this.setState({ isLoading: false })
        // recordEvent(EVENT_TYPE.classes.PAIDCLASS_SCHEDULED_FAILURE, {
        //   studentId: studentId,
        //   course_type: this.selectedCourseType,
        //   sequence_no: selectedSlot?.sequenceNo,
        // });
      });
  }

  bookBoosterClass = (otp) => {
    const { selectedSlot } = this.state;
    const {
      totalClassesCount,
      reviewPopUpStartDate,
    } = this.props;
    const data = {
      teacherId: selectedSlot?.teacherId,
      token: otp,
      courseClassType: SCREEN_TYPE_BOOSTER_CLASS.PROJECT_BOOSTER,
      startTime: selectedSlot?.startTime,
      courseType: this.selectedCourseType,
    }; //

    this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
    let loaderTimer = null;
    if (Platform.OS === IOS_PLATFORM) {
      loaderTimer = setTimeout(() => {
        this.setState({ isLoading: true })
      }, 1000);
    } else {
      this.setState({ isLoading: true })
    }

    const isValid = !isIOS() && InAppReview.isAvailable();
    this.props
      .getBoosterClassBook(this.studentId, data)
      .then((weekWiseData) => {
        if (loaderTimer) {
          clearTimeout(loaderTimer);
        }
        this.setState({ isLoading: false })
        if (weekWiseData) {
          this.props?.getBoosterEligibleToBook(
            this.studentId,
            this.selectedCourseType,
          );
          this.setState({ weekWiseSlotData: weekWiseData?.weekWiseSlotData });
          this.props
            .classesBookedViaDevice(totalClassesCount + 1)
            .then((res) => {
              if (
                res ===
                CLASS_BOOKED_VIA_THIS_DEVICE.CLASS_BOOKED_VIA_THIS_DEVICE &&
                isValid
              ) {
                // recordEvent(EVENT_TYPE.inAppReview.PLAYSTORE_REFERRAL_POPUP);
                if (reviewPopUpStartDate) {
                  if (needToShowReviewPopup()) {
                    setTimeout(() => InAppReview.RequestInAppReview(), 45000);
                  }
                } else {
                  setTimeout(() => InAppReview.RequestInAppReview(), 45000);
                  this.props.reviewPopUpShowAction();
                }
              }
            });
          this.setState({ isLoading: false })
          this.props.updateReloadStatus(true, true);
          this.setState({ isVisible: CLASS_MODAL_TYPES.BOOSTER_CONFIRMED });
          // recordEvent(EVENT_TYPE.classes.PAIDCLASS_SCHEDULED_SUCCESS, {
          //   start_time: selectedSlot?.startTime,
          //   studentId: studentId,
          //   course_type: this.selectedCourseType,
          //   sequence_no: selectedSlot?.sequenceNo,
          //   classType: PROJECT_BOOSTER_EVENT.PROJECT_BOOSTER,
          // });
        }
      })
      .catch(() => {
        this.setState({ isLoading: false })
      });
  };

  _cancelBookedSlot = () => {
    const { selectedSlot } = this.state;
    const { courseItemId } = this.props;
    const cancleClassPayload = {
      teacherId: selectedSlot?.teacherId,
      studentId: this.studentId,
      courseItemId: courseItemId,
      bookingId: selectedSlot?.booking?.id,
    };
    this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
    if (Platform.OS === IOS_PLATFORM) {
      setTimeout(() => {
        this.setState({ isLoading: true })
      }, 1000);
    } else {
      this.setState({ isLoading: true })
    }
    this.props
      .cancelUpcomingStudentClass(cancleClassPayload)
      .then((response) => {
        this.setState({ weekWiseSlotData: response.weekWiseSlotData });
        if (response?.data?.success) {
          // this.setState({weekWiseSlotData: weekWiseData.weekWiseSlotData});
          this.setState({ isLoading: false })
          this.props.updateReloadStatus(true, true);
          this.setState({ isVisible: CLASS_MODAL_TYPES.SLOT_CANCELLED });
          // recordEvent(EVENT_TYPE.classes.PAIDCLASS_CANCELLED_SUCCESS, {
          //   studentId: studentId,
          //   classType: this.props?.route.params.screenType,
          // });
        } else {
          // recordEvent(EVENT_TYPE.classes.PAIDCLASS_CANCELLED_FAILURE, {
          //   studentId: studentId,
          //   course_type: this.selectedCourseType,
          // });
        }
      })
      .catch(() => {
        this.setState({ isLoading: false })
      });
  };

  disableSlotClick(slot) {
    return slot.booking && slot.booking.status === CLASS_TYPE.COMPLETED
      ? true
      : false;
  }

  /**
   * Method to check if slot is available in a day or not
   * @param {*} slots
   */
  _isSlotAvailableInaDay(slots) {
    return slots && slots.length > 0;
  }

  renderDaysColumn(weekDetails) {
    return weekDetails.days.map((day, index) => {
      return (
        <View key={index}>
          <View style={PhoneStyles.dateColumn}>
            <TextWhj
              title={dateFormatter(day.day, DATE_FORMAT_ENUM.Do_MMM)}
              type={TextTypes.bodyThreeBookText}
              style={[
                PhoneStyles.dateTextStyles,
                PhoneStyles.dayBottomTextStyles,
              ]}
            />
            <TextWhj
              type={TextTypes.bodyFourMediumBlackText}
              title={dateFormatter(
                day.day,
                DATE_FORMAT_ENUM.THREE_CHAR_DATE,
              ).toLocaleUpperCase()}
              style={[PhoneStyles.dayTextStyles, PhoneStyles.dayTopTextStyles]}
            />
          </View>
        </View>
      );
    });
  }

  _getTable(weekDetails) {
    return (
      <View style={PhoneStyles.tableContainer}>
        <View style={PhoneStyles.daysContainerStyles}>
          {this.renderDaysColumn(weekDetails)}
        </View>
        <View
          style={[GlobalStyles.separatorView, PhoneStyles.daySeparatorColumn]}
        />
        <View style={PhoneStyles.slotsViewStyle}>
          <HoursSlotsScroll
            weekDetails={weekDetails}
            onSlotPress={this._handleSlotClick}
            screenType={
              this.props?.route?.params?.screenType ||
              SCREEN_TYPE_BOOSTER_CLASS.REGULAR_CLASS
            }
            selectedCourseType={this.selectedCourseType}
          />
        </View>
      </View>
    );
  }

  _selectTeacher = (teacher) => {
    if (Platform.OS === IOS_PLATFORM) {
      setTimeout(() => {
        this.setState({ isLoading: true })
      }, 1000);
    } else {
      this.setState({ isLoading: true })
    }
    this.setState(
      {
        selectedTeacher: teacher,
        isVisible: CLASS_MODAL_TYPES.NO_MODAL,
      },
      async () => {
        getOneTimeSchedulerSlots(
          this.studentId,
          this.state?.selectedTeacher?.id,
        )
          .then((weekWiseData) => {
            this.setState({ weekWiseSlotData: weekWiseData?.weekWiseSlotData });
            // recordEvent(EVENT_TYPE.classes.PAIDCLASS_TEACHER_CHANGED_SUCCESS, {
            //   course_type: this.selectedCourseType,
            //   sequence_no: this.state.selectedSlot?.sequenceNo,
            // });
          })
          .finally(() => {
            this.setState({ isLoading: false })
          });
      },
    );
  };

  _OnSelectTeacher = () => {
    this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL }, () => {
      setTimeout(() => {
        this.setState({ isVisible: CLASS_MODAL_TYPES.TEACHER_SUBSTITUTE });
      }, 2000);
    });
  };

  _renewClick = () => {
    this.setState({
      isVisible: CLASS_MODAL_TYPES.NO_MODAL,
    });
    const url = WEB_URLS.renew(
      isCourseTypeCoding(this.props.courseType)
        ? Configuration.apis.webBaseUrl
        : Configuration.apis.mathWeburl,
      this.props.token,
    );
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        this.props.navigation.navigate(ScreenNames.DashboardWebScreen, {
          url,
          webScreenTitle: '',
          onGoBack: () => {
            updateReloadStatus(true, false);
          },
        });
      } else {
      }
    });
  };
  changeScroll = (scrollHeight) => {
    if (scrollHeight < this.arr[1] && scrollHeight >= this.arr[0]) {
      this.setState({ selectedWeek: WEEK_HEADER_DATA[0] });
    } else if (scrollHeight < this.arr[2] && scrollHeight >= this.arr[1]) {
      this.setState({ selectedWeek: WEEK_HEADER_DATA[1] });
    } else if (
      scrollHeight < this.arr[2] + (this.arr[3] - this.arr[2]) / 3 &&
      scrollHeight >= this.arr[2]
    ) {
      this.setState({ selectedWeek: WEEK_HEADER_DATA[2] });
    } else if (scrollHeight > this.arr[2] + (this.arr[3] - this.arr[2]) / 3) {
      this.setState({ selectedWeek: WEEK_HEADER_DATA[3] });
    }
  };

  maxClassesAllowedInWeek = () => {
    let count = 0;
    const { userConfig } = this.state;
    switch (this.selectedCourseType) {
      case COURSE_TYPE_STATUS.CODING:
        count = userConfig?.maxClassesAllowedInWeek;
        break;
      case COURSE_TYPE_STATUS.MATH:
        count = userConfig?.maxClassesAllowedInWeekForMath;
        break;
      default:
        count = userConfig?.maxClassesAllowedInWeek;
    }
    return count;
  };

  renderScheduleWeek = ({ item, index }) => {
    return (
      <View
        style={PhoneStyles.outerTableContainer}
        onLayout={(event) => {
          const layout = event.nativeEvent.layout;
          this.arr[index + 1] =
            layout.height + (index === 0 ? this.topHeaderHeight : 0);
        }}>
        {index !== 0 ? <View style={PhoneStyles.speretor} /> : null}
        {this.renderWeekHeader(item)}
        {this._getTable(item)}
      </View>
    );
  };

  renderHeader = () => {
    const { selectedTeacher } = this.state;

    return (
      <View
        onLayout={(event) => {
          this.topHeaderHeight = event.nativeEvent.layout.height;
          this.arr[0] = this.topHeaderHeight;
        }}
        style={PhoneStyles.headerContainerStyles}>
        {this?.props?.isPtmNextClass ? (
          <View style={PhoneStyles.ptmBannerContainer}>
            <View style={PhoneStyles.ptmBannerSubContainer}>
              <Image
                source={ptmClassIcon.artwork}
                style={PhoneStyles.ptmBannerIcon}
              />
              <TextWhj
                type={TextTypes.bodyTwoDemiText}
                title={Localisation.ptmClass.bannerNote}
                style={PhoneStyles.ptmBannerText}
              />
            </View>
          </View>
        ) : null}

        <View style={PhoneStyles.recommendationClassesContainer}>
          <TextWhj
            type={TextTypes.bodyTwoDemiText}
            title={Localisation.recommendClassesText(
              this.maxClassesAllowedInWeek(),
            )}
            style={PhoneStyles.slotButtonFontStyles}
          />
        </View>
        <View style={PhoneStyles.teacherContainer}>
          <TextWhj
            type={TextTypes.bodyTwoBookText}
            title={Localisation.yourTeacher}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({
                isVisible: CLASS_MODAL_TYPES.TEACHER_SUBSTITUTE,
              });
            }}>
            <View style={PhoneStyles.teacherSelector}>
              <Image
                source={ScheduleScreenImage.learningIcon}
                style={PhoneStyles.teacherSelectorLeftImage}
              />
              <TextWhj
                type={TextTypes.bodyThreeMediumBlackText}
                title={selectedTeacher?.name}
                style={PhoneStyles.slotButtonFontStyles}
                numberOfLines={2}
              />
              <Image
                source={ScheduleScreenImage.expandMore}
                style={PhoneStyles.teacherSelectorRightImage}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    const { selectedTeacher, isVisible, teacherList, isLoading } = this.state;
    // const { teacherList } = this.props;
    const data = this.state?.weekWiseSlotData || [];
    return (
      <View style={PhoneStyles.container}>
        <View style={PhoneStyles.mainContentContainer}>
          <SectionList
            ref={(ref) => (this.sectionListRef = ref)}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={this.renderHeader}
            onScroll={this.handleScroll}
            renderSectionHeader={() => (
              <View style={[PhoneStyles.weekHeader]}>
                {data?.length > 0 ? this.renderWeekElement() : null}
              </View>
            )}
            stickySectionHeadersEnabled
            sections={[{ data }]}
            renderItem={this.renderScheduleWeek}
            keyExtractor={(_, i) => `week-${i}`}
          />
        </View>
        {isVisible === CLASS_MODAL_TYPES.TEACHER_SUBSTITUTE && (
          <SelectTeacher
            setActionModalVisible={() =>
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL })
            }
            teacherList={teacherList}
            selectedTeacher={selectedTeacher}
            selectTeacher={this._selectTeacher}
            defaultTeacherId={this.teacherId}
          />
        )}
        {isVisible === CLASS_MODAL_TYPES.SCHEDULE_CLASS && (
          <ScheduleClassPopup
            screenType={this.props?.route?.params?.screenType}
            data={this.state.selectedSlotData}
            onSelectTeacher={() => {
              this._OnSelectTeacher();
            }}
            selectedCourseType={this.selectedCourseType}
            bookUpcomingSlot={(otp) => {
              // this.bookBoosterClass(otp);
            }}
            bookSlot={() => {
              this._bookUpcomingSlot();
            }}
            setActionModalVisible={() => {
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
            }}
            isSubstitute={
              this.state?.selectedTeacher?.id !== this.teacherId
            }
          />
        )}
        {isVisible === CLASS_MODAL_TYPES.COVID_19_THRESHOLD && (
          <Covid19LockdownPopup
            setActionModalVisible={() =>
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL })
            }
            teacherList={teacherList}
            selectedTeacher={selectedTeacher}
            closeModal={() => {
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
            }}
            maxClassesAllowedInWeek={this.maxClassesAllowedInWeek()}
          />
        )}

        {isVisible === CLASS_MODAL_TYPES.SLOT_CONFIRMED && (
          <ClassScheduledConfirmedPopup
            classSchedule={Localisation.yayClassScheduled}
            selectedSlot={this.state.selectedSlotData}
            setActionModalVisible={() => {
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
            }}
          />
        )}
        {isVisible === CLASS_MODAL_TYPES.SLOT_CANCELLED && (
          <SlotCancelledConfirmation
            startTime={this.state.selectedSlotData.startTime}
            setActionModalVisible={() => {
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
            }}
          />
        )}
        {isVisible === CLASS_MODAL_TYPES.BOOSTER_CONFIRMED && (
          <ClassScheduledConfirmedPopup
            classSchedule={
              this.selectedCourseType == COURSE_TYPE_STATUS.MATH
                ? Localisation.worksheetClassConfirmation
                : Localisation.boosterClassConfirmation
            }
            selectedSlot={this.state.selectedSlotData}
            setActionModalVisible={() => {
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
            }}
          />
        )}
        {isVisible === CLASS_MODAL_TYPES.CANCEL_SLOT_CLASS && (
          <UpcomingClassDetail
            data={this.state.selectedSlotData}
            setActionModalVisible={(value) =>
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL })
            }
            defaultTeacherId={this.props.teacherId}
            cancelBookedSlot={this._cancelBookedSlot}
            showClassDescription={false}
            selectedCourseType={this.selectedCourseType}
          />
        )}
        {isVisible === CLASS_MODAL_TYPES.NO_CREDIT_LEFT && (
          <NoCreditLeftPopup
            onRenewClick={() => this._renewClick()}
            setActionModalVisible={() => {
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
            }}
          />
        )}
        {isVisible === CLASS_MODAL_TYPES.DAY_LIMIT_REACHED && (
          <DayLimitPopup
            setActionModalVisible={() => {
              this.setState({ isVisible: CLASS_MODAL_TYPES.NO_MODAL });
            }}
          />
        )}
        {this.props?.route?.params?.isMenuModal && (
          <HeaderMenu
            setActionModalVisible={() =>
              this.props?.navigation?.setParams({isMenuModal: false})
            }
            navigation={this.props?.navigation}
          />
        )}
        {isLoading ? <Loader /> : null}
      </View>
    );
  }
}

export default PaidScheduler;
// const mapStateToProps = (state) => {
//   const userInfo = state.userInfo || {};
//   const scheduleClass = state.scheduleClass || {};
//   const oneTimeScheduler = state.oneTimeScheduler || {};
//   const courseType = state.userInfo?.selectedCourseType;
//   const courseItemId = userInfo?.userInfo?.courseItemId;
//   const mathCourseItemId = userInfo?.userInfo?.mathCourseItemId;
//   const getCourseType = state?.userInfo?.selectedCourseType;
//   const teacherId = state?.userInfo?.userInfo?.teacherId;
//   const mathTeacherId = state?.userInfo?.userInfo?.mathTeacherId;
//   const codingCourseCredits = state?.userInfo?.userInfo?.credits;
//   const mathCourseCredits = state?.userInfo?.userInfo?.mathCredits;
//   const boosterClass = state.oneTimeScheduler?.classCourseType;

//   return {
//     token: userInfo?.token,
//     studentId: userInfo?.userInfo?.id,
//     courseItemId: isCourseTypeCoding(courseType)
//       ? courseItemId
//       : mathCourseItemId,
//     credits: isCourseTypeCoding(courseType)
//       ? codingCourseCredits
//       : mathCourseCredits,
//     gettingTeacherList: scheduleClass.gettingTeacherList,
//     teacherListCallFailed: scheduleClass.teacherListCallFailed,
//     teacherList: scheduleClass.teacherList,
//     oneTimeSchedulerSlots: oneTimeScheduler.oneTimeSchedulerSlots,
//     teacherId: isCourseTypeCoding(courseType) ? teacherId : mathTeacherId,
//     courseType: courseType,
//     selectedCourseType: getCourseType,
//     timeZone: userInfo?.userInfo?.timezone,
//     totalClassesCount: userInfo?.totalClassesCount,
//     reviewPopUpStartDate: state?.userInfo?.reviewPopUpStartDate,
//     maxClassesAllowedInWeek: state?.BookingLimitReducer?.recommendData,
//     maxClassesAllowedInWeekForMath: state?.BookingLimitReducer?.recommendData,
//     boosterClass: boosterClass,
//     isPtmNextClass: state?.classReducer?.nextClass?.isNextClassPtm,
//   };
// };

// export default connect(mapStateToProps, {
//   getSubstituteTeacherForOneTimeScheduler,
//   getOneTimeSchedulerSlots,
//   scheduleOnTimeSlot,
//   cancelUpcomingStudentClass,
//   updateReloadStatus,
//   classesBookedViaDevice,
//   reviewPopUpShowAction,
//   getBoosterClassBook,
//   getBoosterEligibleToBook,
// })(ScheduleClass);
