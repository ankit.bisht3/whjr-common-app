import { StyleSheet } from 'react-native';
import Colors from '../../../../common/Colors';
import { FontName } from '../../../../utility/FontsUtil';
import { isIOS } from '../../../../utility';
import { widthPercentageToDP } from '../../../../utility/SizeUtil';

const PhoneStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteWhj,
    },
    mainContentContainer: {
        marginVertical: 16,
    },
    teacherContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 16,
        marginHorizontal: 16,
    },
    weekText: {
        color: Colors.blackWhj,
    },
    weekTextMath: {
        color: Colors.blueWhj,
    },
    yourTeacherStyles: {
        color: Colors.greyDarkWhj,
    },
    teacherSelectorLeftImage: {
        width: 18,
        height: 18,
        marginRight: 8,
        alignSelf: 'center',
    },
    weekTopSeparatorStyles: {
        marginVertical: 12,
        marginTop: 16,
    },
    outerTableContainer: {
        zIndex: 0,
        elevation: 0,
    },
    teacherSelectorRightImage: {
        width: 18,
        height: 18,
        marginLeft: 8,
        marginTop: 0,
        alignSelf: 'center',
    },
    recommendationClassesContainer: {
        flexDirection: 'row',
        marginTop: 16,
        marginHorizontal: 16,
    },
    headerContainerStyles: {
        borderBottomWidth: 0,
        borderColor: Colors.separatorWhj,
        paddingBottom: 16,
        zIndex: 5,
        // elevation: 5,
        backgroundColor: '#fff',
    },
    teacherSelector: {
        flexDirection: 'row',
        backgroundColor: Colors.blueLightWhj,
        borderRadius: 20,
        alignItems: 'center',
        height: 36,
        paddingHorizontal: 10,
        width: '75%',
        marginLeft: 10,
    },
    selectedWeekFontStyles: {
        color: Colors.orangeWhj,
    },
    weekFontStyles: {
        fontSize: 16,
        color: Colors.greyDarkWhj,
    },
    weekContainerStyles: {
        flex: 1,
        flexDirection: 'row',
    },
    weekHeaderStyles: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingHorizontal: 8,
        paddingVertical: 8,
        height: 34,
        marginLeft: 16,
        backgroundColor: Colors.greyVeryLightWhj,
        borderRadius: 6,
        marginRight: 8,
        marginBottom: 8,
        color: Colors.lightBlackTextWhj,
    },
    dayTextStyles: {
        color: Colors.greyDarkWhj,
        fontSize: 10,
        fontFamily: FontName.whjMedium,
        alignSelf: 'flex-start',
    },
    dateTextStyles: {
        color: Colors.blackTextWhj,
        fontSize: 14,
        alignSelf: 'flex-start',
    },
    dayTopTextStyles: {
    },
    dayBottomTextStyles: {
    },
    daysContainerStyles: {
        marginRight: 10,
    },
    slotButtonStyles: {
        backgroundColor: Colors.whiteWhj,
        borderColor: Colors.noSlotBorderColor,
    },
    commonSlotStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        height: 40,
        width: 70,

        borderWidth: 0.35,
        borderColor: Colors.noSlotBorderColor,
        backgroundColor: Colors.whiteWhj,
    },
    selectedSlotButtonStyles: {
        backgroundColor: Colors.orangeWhj,
        borderColor: Colors.orangeWhj,
    },
    selectedMathSlotButtonStyles: {
        backgroundColor: Colors.blueWhj,
        borderColor: Colors.blueWhj,
    },
    slotButtonFontStyles: {
        color: Colors.blackTextWhj,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '93%'
    },
    newSlotFont: {
        color: Colors.greyDarkWhj,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    selectedSlotButtonFontStyles: {
        color: '#ffffff',
    },
    selectedMathSlotButtonFontStyles: {
        color: Colors.whiteWhj,
    },
    daySlotContainerStyles: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingRight: 30,
        // marginVertical: 10,
    },
    tableContainer: {
        flexDirection: 'row',
        marginVertical: 0,
        marginLeft: 16,
    },
    slotParentContainer: {
        marginTop: 8,
        // marginLeft: 16,
        overflow: 'hidden',
    },
    daySeparatorColumn: {
        marginRight: 8,
    },
    slotsViewStyle: {
        width: widthPercentageToDP('82%'),
    },
    slotStyle: {
        marginHorizontal: 4,
        paddingVertical: 8,
        alignItems: 'center',
        justifyContent: 'center',
    },
    ClassCompletedStyle: {
        backgroundColor: Colors.greyDisabledWhj,
        borderColor: Colors.greyDisabledWhj,
    },
    ClassCompletedFontStyle: {
        color: Colors.blackTextWhj,
    },
    noSlotSyle: {
        marginLeft: 4,
    },
    ptmBannerContainer: {
        paddingVertical: 10,
        width: '100%',
        backgroundColor: Colors.darkSkyblue,
    },
    ptmBannerSubContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 16,
    },
    ptmBannerIcon: { height: 45, width: 55 },
    ptmBannerText: { marginLeft: 8, color: Colors.whiteWhj, paddingRight: 50 },
    dateColumn: {
        height: 40,
        justifyContent: 'space-between',
        marginVertical: 8,
    },
    speretor: {
        height: 1,
        flex: 1,
        backgroundColor: Colors.greyPopupBorderColor,
        marginBottom: 16,
        marginTop: 16,
    },
    weekHeader: {
        flexDirection: 'row',
        backgroundColor: Colors.whiteWhj,
        width: '100%',
        shadowOpacity: 0.8,
        marginBottom: 8,
        shadowOffset: {
            height: 2,
        },
        shadowColor: 'rgb(240,240,240)',
        elevation: isIOS() ? 0 : 2,
    },
    weekHeaderTouch: { flex: 1, paddingVertical: 16, justifyContent: 'center' },
});

export default PhoneStyles;
