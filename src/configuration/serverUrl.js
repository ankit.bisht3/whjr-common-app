import ProdConfig from '../configuration/ProdConfig';
import StageConfig from '../configuration/StagConfig';
import UatConfig from '../configuration/UatConfig';

export const SERVER_URLS = {
  PRODUCTION: ProdConfig,
  STAGE: StageConfig,
  UAT: UatConfig,
};
