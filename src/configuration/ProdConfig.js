import { AppEnvironmentEnum } from "../constants/enum";

const ProdConfig = {
  environment: AppEnvironmentEnum.PRODUCTION,
  apis: {
    apiBaseUrl: 'https://code.whitehatjr.com/api/V1'
  }
};

export default ProdConfig;
