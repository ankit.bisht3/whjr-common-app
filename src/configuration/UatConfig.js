import { AppEnvironmentEnum } from "../constants/enum";

const UatConfig = {
  environment: AppEnvironmentEnum.UAT,
  apis: {
    apiBaseUrl: 'https://qa-api.whjr.one/api/V1'
  }
};
export default UatConfig;
