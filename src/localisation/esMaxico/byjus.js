import { IS_BYJUS_APP } from '../../configuration/byjus';

const website = 'www.byjusfutureschool.com';
const optoutEmail = 'optout@byjusfutureschool.com';
const appName = IS_BYJUS_APP ? "BYJU'S FutureSchool" : 'WhiteHat Jr';

export default {
  appName,
};
