import { IS_BYJUS_APP } from '../../configuration/byjus';

const appName = IS_BYJUS_APP ? "BYJU'S FutureSchool" : 'WhiteHat Jr';
const website = IS_BYJUS_APP
  ? 'www.byjusfutureschool.com'
  : 'www.whitehatjr.com';
export const optoutEmail = IS_BYJUS_APP
  ? 'optout@byjusfutureschool.com'
  : 'optout@whitehatjr.com';

export default {
  appName
};
