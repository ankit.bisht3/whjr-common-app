import { IS_BYJUS_APP } from '../../configuration/byjus';

const appName = IS_BYJUS_APP ? "BYJU'S FutureSchool" : 'WhiteHat Jr';
const website = 'www.byjusfutureschool.com';
const optoutEmail = 'optout@byjusfutureschool.com';

export default {
  appName,
};
