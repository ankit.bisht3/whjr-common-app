import { IS_BYJUS_APP } from '../../configuration/byjus';
const appName = IS_BYJUS_APP ? "BYJU'S FutureSchool" : 'WhiteHat Jr';

const Common = {
  recommendClassesText: (limit) => `Recomendamos ${limit} aulas por semana`,
};

export default Common;
