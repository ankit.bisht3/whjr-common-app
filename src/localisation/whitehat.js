const appName = 'WhiteHat Jr';
const website = 'www.whitehatjr.com';
const optoutEmail = 'optout@whitehatjr.com';

export default {
  appName,
};
