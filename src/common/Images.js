import CommonImageStyle from './CommonImageStyle';

export const ScheduleScreenImage = {
    learningIcon: require('../assets/images/icon-learning-with-a-book.png'),
    expandMore: require('../assets/images/ic_expand_more.png'),
    blackWhiteStrips: require('../assets/images/black_white_strips.png'),
};

export const ptmClassIcon = {
    artwork: require('../assets/images/artwork.png'),
};

export const ClassPopupsImages = {
    closeDialog: require('../assets/images/close-dialog-icon.png'),
    blackCalendar: require('../assets/images/black_calender.png'),
    blackClock: require('../assets/images/black_clock.png'),
    teacherIcon: require('../assets/images/teacher-icon.png'),
    blackEmail: require('../assets/images/black-email-icon.png'),
    covidImage: require('../assets/images/covid.png'),
    classConfirmedEmoji: require('../assets/images/class-scheduled.png'),
    classCancelled: require('../assets/images/slot-cancelled.png'),
    uncheckedCheckbox: require('../assets/images/unchecked-checkbox.png'),
    checkedCheckbox: require('../assets/images/checked-checkbox.png'),
    blueCheckedCheckbox: require('../assets/images/blue-check-box.png'),
};

export const BoosterImages = {
    arrowDropDown: require('../assets/images/arrowDropDown.png'),
    boosterIcon: require('../assets/images/boosterIcon.png'),
};

export const LoginScreenImages = {
    lockIcon: require('../assets/images/lock-icon.png')
};


export const AuthScreenImages = {
    lockIcon: {
        source: LoginScreenImages.lockIcon,
        style: CommonImageStyle.lockIcon,
    }
};

export const CommonImages = {
    orangeTick: require('../assets/images/orangeTick.png'),
};

export const ClassScreenImages = {
    blueRectangleIcon: require('../assets/images/blue-rectangle-icon.png'),
    techIcon: require('../assets/images/tech-icon.png'),
};

export const LoaderImages = {
    GenericLoader: require('../assets/images/loader/generic-loader.json'),
};
