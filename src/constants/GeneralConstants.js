export const WEEKS = [
    'Week 1',
    'Week 2',
    'Week 3',
    'Week 4',
    'Week 5',
    'Week 6',
];

export const IOS_PLATFORM = 'ios';
export const WEEK = 'week';
export const DAYS = 'days';

export const DUMMY_STUDENT_ID = "10df215c-0564-44be-947d-7376ec58f95e";
export const DUMMY_TEACHER_ID = "bf955657-1d23-4477-b9cf-7eee2d39435f";

export const MAX_SLOT_BOOKING_LIMIT_WEEK = 5;
export const MAX_SLOT_BOOKING_LIMIT_DAY = 1;
