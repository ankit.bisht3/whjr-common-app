import { MAX_SLOT_BOOKING_LIMIT_WEEK, START_CLASS_IMMEDIATELY } from '../../constants/GeneralConstants';
import { checkResourceNotNullOrEmpty } from '../../utility/ValidationUtil';

export const GlobalConfigModel = (globalData) => {
    let payload = {};
    let appsBannerData = globalData?.configs?.APPS_CAROUSEL;

    if (appsBannerData && appsBannerData.length) {
        payload.appsCarousel = appsBannerModel(appsBannerData);
    }
    payload.studentHelpDeskPaid = globalData?.configs?.studentHelpDesk;
    payload.studentHelpDeskTrial = globalData?.configs?.studentHelpDesk_Trial;

    payload.showClassBeforeTime = globalData?.configs
        ?.STUDENT_NEXT_CLASS_JOIN_DURATION
        ? globalData?.configs?.STUDENT_NEXT_CLASS_JOIN_DURATION
        : globalData?.configs?.TEACHER_NEXT_CLASS_JOIN_DURATION;

    payload.mobileAvailableClasses = globalData?.configs?.mobileAvailableClasses;
    payload.parentReview = globalData?.configs?.review;
    payload.parentMathReview = globalData?.configs?.review_MATH;
    payload.parentMusicReview = globalData?.configs?.review_MUSIC; //TODO
    payload.maxClassesAllowedInWeek = globalData?.configs?.PAID_CLASS_MAX
        ? globalData?.configs?.PAID_CLASS_MAX
        : MAX_SLOT_BOOKING_LIMIT_WEEK;
    payload.maxClassesAllowedInWeekForMath = globalData?.configs
        ?.STUDENT_MAX_WEEKLY_BOOKING_LIMIT
        ? globalData?.configs?.STUDENT_MAX_WEEKLY_BOOKING_LIMIT
        : MAX_SLOT_BOOKING_LIMIT_WEEK;

    payload.trackingCode = globalData?.trackingCode;
    payload.deviceInfo = deviceInfoModel(globalData?.device);
    payload.abConfig = convertABResponseToModel(globalData?.abTests);
    payload.projectBoosterEligibilityCount = checkResourceNotNullOrEmpty(
        globalData?.configs?.projectBoosterEligibilityCount,
    );

    payload.appUpdate = globalData?.configs?.MOBILE_APP_UPDATE;

    if (globalData?.abTests) {
        payload.immediateClassConfigType = getimmediateClassConfigType(
            globalData?.abTests,
        );
    } else {
        payload.immediateClassConfigType = {};
    }

    payload.PK_QUIZ_CONFIG = quizConfigModal(globalData?.configs?.PK_QUIZ_CONFIG || {});

    return payload;
};

const quizConfigModal = (obj) => {

    let payload = {};

    payload.questionDurationInSec = obj?.questionDurationInSec ?? 0;
    payload.whitelistedCountries = obj?.whitelistedCountries?.map(it => it) ?? [];
    payload.whitelistedCourseShortNameToClassType = {};

    for (let key in (obj?.whitelistedCourseShortNameToClassType || {})) {
        payload.whitelistedCourseShortNameToClassType[key] = obj?.whitelistedCourseShortNameToClassType?.[key]?.map(it => it) ?? [];
    }

    return payload;
}

const deviceInfoModel = (deviceInfo) => {
    let payload = {};
    payload.id = deviceInfo.id;
    payload.userId = deviceInfo.userId;
    return payload;
};

const convertABResponseToModel = (abCourseData) => {
    let list = [];
    for (let data of abCourseData) {
        list.push(new abConfigData(data));
    }
    return list;
};

const abConfigData = (configData) => {
    let payload = {};
    payload.name = configData.name;
    payload.id = configData.id;
    payload.variant = configData.variant;
    payload.configKey = configData.configKey;
    return payload;
};

const appsBannerModel = (appsBannerData) => {
    const dataArr = [];
    appsBannerData?.forEach((element) => {
        const dataObj = {};
        dataObj.image = checkResourceNotNullOrEmpty(element.image);
        dataObj.deeplink = checkResourceNotNullOrEmpty(element.deepLink);

        dataArr.push(dataObj);
    });
    return dataArr;
};

const getimmediateClassConfigType = (configData) => {
    let config = {};
    let index = configData.findIndex(
        (item) => item.name === START_CLASS_IMMEDIATELY,
    );
    if (index !== -1) {
        config = configData[index];
    }
    return config;
};
