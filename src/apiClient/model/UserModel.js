
import {
    checkResourceNotNullElseZero,
    checkResourceNotNullOrEmpty,
} from '../../utility/ValidationUtil';
import { COURSE_TYPE_STATUS } from '../../constants/enum';
export const UserModel = (userData) => {
    const userInfo = userData?.data?.data;
    const userStudentData = userInfo?.students?.[0];
    const mathCourse = COURSE_TYPE_STATUS.MATH;
    const codingCourse = COURSE_TYPE_STATUS.CODING;
    let payload = {};
    payload.token = checkResourceNotNullOrEmpty(userInfo.token);
    payload.tokenVersion = checkResourceNotNullOrEmpty(userInfo.tokenVersion);
    payload.updatedById = checkResourceNotNullOrEmpty(userInfo.updatedById);
    payload.updatedByType = checkResourceNotNullOrEmpty(userInfo.updatedById);
    payload.userRoles = checkResourceNotNullOrEmpty(userInfo.userRoles);
    payload.userVersion = checkResourceNotNullOrEmpty(userInfo.userVersion);
    payload.userAddresses = checkResourceNotNullOrEmpty(userInfo.userAddresses);
    payload.otp = checkResourceNotNullElseZero(userInfo?.otp);
    payload.additionalDialCode = checkResourceNotNullOrEmpty(
        userStudentData?.additionalDialCode,
    );
    payload.additionalContactNo = checkResourceNotNullOrEmpty(
        userStudentData?.additionalContactNo,
    );
    payload.additionalEmail = checkResourceNotNullOrEmpty(
        userStudentData.additionalEmail,
    );
    payload.city = checkResourceNotNullOrEmpty(userStudentData.city);
    payload.classType = checkResourceNotNullOrEmpty(userStudentData.class_type);
    payload.codeorgPassword = checkResourceNotNullOrEmpty(
        userStudentData.codeorgPassword,
    );
    payload.codeorgUserName = checkResourceNotNullOrEmpty(
        userStudentData.codeorgUserName,
    );
    payload.countryCode = checkResourceNotNullOrEmpty(
        userStudentData.countryCode,
    );
    payload.defaultLang = checkResourceNotNullOrEmpty(userInfo.defaultLang);
    payload.courseShortName = checkResourceNotNullOrEmpty(
        userStudentData.course_short_name,
    );
    payload.dateOfBirth = checkResourceNotNullOrEmpty(
        userStudentData.dateOfBirth,
    );
    payload.gender = checkResourceNotNullOrEmpty(userStudentData.gender);
    payload.grade = checkResourceNotNullOrEmpty(userStudentData.grade);
    payload.gradeUpdatedAt = checkResourceNotNullOrEmpty(
        userStudentData.gradeUpdatedAt,
    );
    payload.hobby = checkResourceNotNullOrEmpty(userStudentData.hobby);
    payload.isCodeorgCreated = checkResourceNotNullOrEmpty(
        userStudentData.isCodeorgCreated,
    );
    payload.isLaptop = checkResourceNotNullOrEmpty(userStudentData.isLaptop);
    payload.isMigrationSynced = checkResourceNotNullOrEmpty(
        userStudentData.isMigrationSynced,
    );
    payload.isNotificationSubscribed = checkResourceNotNullOrEmpty(
        userStudentData.isNotificationSubscribed,
    );
    payload.isOnetimeAllowed = checkResourceNotNullOrEmpty(
        userStudentData.isOnetimeAllowed,
    );
    payload.isOtpOnCancellation = checkResourceNotNullOrEmpty(
        userStudentData.isOtpOnCancellation,
    );
    payload.isProgressOpened = checkResourceNotNullOrEmpty(
        userStudentData.isProgressOpened,
    );
    payload.latitude = checkResourceNotNullOrEmpty(userStudentData.latitude);
    payload.longitude = checkResourceNotNullOrEmpty(userStudentData.longitude);
    payload.dialCode = checkResourceNotNullOrEmpty(userStudentData.dialCode);
    payload.email = checkResourceNotNullOrEmpty(userStudentData.email);
    payload.mobile = checkResourceNotNullOrEmpty(userStudentData.mobile);
    payload.name = checkResourceNotNullOrEmpty(userStudentData.name);
    payload.paidStatus = checkResourceNotNullOrEmpty(userStudentData.paidStatus);
    payload.parentName = checkResourceNotNullOrEmpty(userStudentData.parentName);
    payload.postalCode = checkResourceNotNullOrEmpty(userStudentData.postalCode);
    payload.principalEmail = checkResourceNotNullOrEmpty(
        userStudentData.principalEmail,
    );
    payload.profileImageUrl = checkResourceNotNullOrEmpty(
        userStudentData.profileImageUrl,
    );
    payload.recordStatus = checkResourceNotNullOrEmpty(
        userStudentData.recordStatus,
    );
    payload.referralCode = checkResourceNotNullOrEmpty(
        userStudentData.referralCode,
    );
    payload.referralType = checkResourceNotNullOrEmpty(
        userStudentData.referralType,
    );
    payload.schoolName = checkResourceNotNullOrEmpty(userStudentData.schoolName);
    payload.state = checkResourceNotNullOrEmpty(userStudentData.state);
    payload.studentEmail = checkResourceNotNullOrEmpty(
        userStudentData.studentEmail,
    );
    payload.studentCourses = checkResourceNotNullOrEmpty(
        userStudentData.studentCourses,
    );
    payload.timezone = checkResourceNotNullOrEmpty(userStudentData.timezone);
    payload.trialStatus = checkResourceNotNullOrEmpty(
        userStudentData.trialStatus,
    );
    payload.userId = checkResourceNotNullOrEmpty(userStudentData.userId);
    payload.userReferralCode = checkResourceNotNullOrEmpty(userInfo.referralCode);
    payload.userReferralLink = checkResourceNotNullOrEmpty(
        userStudentData.userReferralLink,
    );
    payload.websiteUserName = checkResourceNotNullOrEmpty(
        userStudentData.websiteUserName,
    );
    payload.id = checkResourceNotNullOrEmpty(userStudentData.id);
    payload.userStudentId = checkResourceNotNullOrEmpty(userStudentData.id);
    payload.mobileVerified = checkResourceNotNullOrEmpty(
        userStudentData.mobileVerified,
    );
    payload.password = checkResourceNotNullOrEmpty(userStudentData.password);
    payload.passwordStrength = checkResourceNotNullOrEmpty(
        userStudentData.passwordStrength,
    );
    payload.socialLoginId = checkResourceNotNullOrEmpty(
        userStudentData.socialLoginId,
    );
    if (
        userStudentData?.student_courses &&
        userStudentData.student_courses.length > 0
    ) {
        payload.student_courses = userStudentData.student_courses;
    }
    payload.socialLoginType = checkResourceNotNullOrEmpty(
        userStudentData.socialLoginType,
    );
    if (
        userStudentData?.student_courses &&
        userStudentData.student_courses.length > 0
    ) {
        payload.credits = checkResourceNotNullOrEmpty(
            getCreditsFromCourses(userStudentData?.student_courses, codingCourse),
        );
        payload.mathCredits = checkResourceNotNullOrEmpty(
            getCreditsFromCourses(userStudentData?.student_courses, mathCourse),
        );
        payload.teacherId = checkResourceNotNullOrEmpty(
            getdefaultTeacherFromCourses(
                userStudentData?.student_courses,
                codingCourse,
            ),
        );
        payload.mathTeacherId = checkResourceNotNullOrEmpty(
            getdefaultTeacherFromCourses(
                userStudentData?.student_courses,
                mathCourse,
            ),
        );
    }
    return payload;
};

const getdefaultTeacherFromCourses = (data = [], courseType) => {
    if (data.length > 0) {
        return data.reduce(
            (itemId, course) =>
                course?.courseType === courseType ? course?.teacher?.id : itemId,
            '',
        );
    }
    return '';
};
const getCreditsFromCourses = (data = [], courseType) => {
    if (data.length > 1) {
        return data.reduce(
            (itemId, course) =>
                course?.courseType === courseType ? course?.credits : itemId,
            '',
        );
    }
    return data[0]?.credits;
};
