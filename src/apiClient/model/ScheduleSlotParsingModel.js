import { slotModel } from './SlotModel';
import moment from 'moment-timezone';
import { WORKING_HOUR_RANGE } from '../../constants/enum';
import { format, momentLocale } from '../../utility/DateUtil';
import { DATE_FORMAT_ENUM, SCHEDULE_WEEK_TITLE_ENUM } from '../../constants/enum';
import { WEEK, DAYS } from '../../constants/GeneralConstants';
import { dateFormatter } from '../../utility';
import { IS_BYJUS_APP } from '../../configuration/byjus';

export const scheduleSlotParsingModel = (slotInfo, timezone) => {
    let payload = {
        oneTimeSchedulerSlots: [],
        weekWiseSlotData: [],
    };

    payload.oneTimeSchedulerSlots = convertResponseToModel(
        slotInfo?.slots,
        timezone,
    );

    payload.oneTimeSchedulerSlots = getSlotsInWorkingRange(
        payload?.oneTimeSchedulerSlots,
        timezone,
    );
    console.log("payloadpayload", payload)

    payload.weekWiseSlotData = weekWiseSlotFormatting(
        payload?.oneTimeSchedulerSlots,
        timezone,
    );

    payload.oneTimeSchedulerSlots = [];
    return payload;
};

const convertResponseToModel = (slotdata, timezone) => {
    let list = [];
    for (let slot of slotdata) {
        list.push(new slotModel(slot, timezone));
    }
    return list;
};

const getSlotsInWorkingRange = (list, timezone) => {
    const userCountry =
        moment().tz(timezone).utcOffset() === 330 ? 'IN' : 'default';
    console.log("userCountryuserCountry", list)
    const workingHour = getWorkingHour(userCountry);

    const currentTime = moment().tz(timezone).format();
    let filteredList = list?.filter(
        (val) =>
            isBookingExists(val) ||
            (!isSameDay(moment().tz(timezone), moment(val?.startTime).tz(timezone)) &&
                moment
                    .duration(
                        moment(val?.startTime).tz(timezone).format(DATE_FORMAT_ENUM.HH_MM),
                    )
                    .asMinutes() >= workingHour.startMinute &&
                moment
                    .duration(
                        moment(val?.startTime).tz(timezone).format(DATE_FORMAT_ENUM.HH_MM),
                    )
                    .asMinutes() <= workingHour.endMinute &&
                moment(val?.startTime)
                    .tz(timezone)
                    .isAfter(moment(currentTime).tz(timezone))),
    );
    console.log("filteredListfilteredList", filteredList)
    return filteredList;
};

const isBookingExists = (val) => val?.booking && val?.booking?.id;

export const getWorkingHour = (country) => {
    if (country) {
        return WORKING_HOUR_RANGE[country];
    }
};

/**
 * check if date in between 2 dates
 * @param {base date} date
 * @param {start date to compare} startDate
 * @param {end date to compare } endDate
 */
export const isSameDay = (date, startDate) => {
    return format(date) === format(startDate);
};

const weekWiseSlotFormatting = (slots, timeZone) => {
    let formattedData = [
        {
            week: SCHEDULE_WEEK_TITLE_ENUM.WEEK_1,
            days: [],
        },
        {
            week: SCHEDULE_WEEK_TITLE_ENUM.WEEK_2,
            days: [],
        },
        {
            week: SCHEDULE_WEEK_TITLE_ENUM.WEEK_3,
            days: [],
        },
        {
            week: SCHEDULE_WEEK_TITLE_ENUM.WEEK_4,
            days: [],
        },
    ];
    if (IS_BYJUS_APP) {
        formattedData = [
            ...formattedData,

            {
                week: SCHEDULE_WEEK_TITLE_ENUM.WEEK_5,
                days: [],
            },
            {
                week: SCHEDULE_WEEK_TITLE_ENUM.WEEK_6,
                days: [],
            },
        ];
    }
    let day;
    for (let i = 0; i < formattedData?.length; i++) {
        for (let j = 0; j < 7; j++) {
            let date = parser(timeZone, i, j);
            formattedData[i].days.push({
                day: date,
                slots: [],
            });
        }
    }
    for (let k = 0; k < slots?.length; k++) {
        day = moment(slots[k].startTime).tz(timeZone);

        for (let i = 0; i < formattedData?.length; i++) {
            for (let j = 0; j < formattedData[i].days.length; j++) {
                if (
                    dateFormatter(
                        formattedData[i].days[j].day,
                        DATE_FORMAT_ENUM.DD_MM_YYYY,
                    ) === dateFormatter(day, DATE_FORMAT_ENUM.DD_MM_YYYY)
                ) {
                    formattedData[i].days[j].slots.push(slots[k]);
                }
            }
        }
    }
    return formattedData;
};

export const parser = (timeZone, i, j) => {
    return customMoment()
        .startOf('week')
        .isoWeekday(1)
        .tz(timeZone)
        .add(i, WEEK)
        .add(j, DAYS);
};

export const customMoment = momentLocale();
