import axios from 'axios';
import { Platform } from 'react-native';
import Configuration from '../configuration';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { COMMON_AXIOS_QUERIES } from '../common/AsyncKeys';
import { getAxiosCommonParams, saveUserInfo, saveUserGlobalConfig } from '../utility';
import { getGeoInfo, getGlobalConfiguration, getUserConfig } from './ApiCalls';
import { UserModel } from './model/UserModel';
import { GlobalConfigModel } from './model/GlobalConfigModel';


export default class ApiClient {
    constructor() {
        if (!ApiClient.instance) {
            ApiClient.instance = this;
        }
        return ApiClient.instance;
    }

    saveCommonQueriesAndHeaders = (commonQueries, commonHeaders) => {
        const query = {
            commonQueries,
            commonHeaders
        }
        AsyncStorage.setItem(COMMON_AXIOS_QUERIES, JSON.stringify(query))
    }

    fetchUserConfig = async () => {
        try {
            const getUserInfo = await getUserConfig();
            let payload = {};
            payload = UserModel(getUserInfo);
            saveUserInfo(payload);
            this.getGlobalConfig(payload);
            return payload;
        } catch (error) {
        }
    };


    getGlobalConfig = async (userInfo) => {
        const countryCode = userInfo?.countryCode;
        try {
            if (!countryCode) {
                getGeoInfo().then((res) => {
                    this.getConfigModal(res?.data?.data?.countryIsoCode);
                });
            } else {
                this.getConfigModal(countryCode);
            }
        } catch (error) {
            return error;
        }
    };

    getConfigModal = async (countryCode) => {
        try {
            const body = JSON.stringify({
                device: {
                    browser: Platform.OS,
                    browserVersion: Platform.Version,
                    deviceType: "mobile",
                    deviceOs: Platform.OS,
                    osVersion: Platform.Version,
                    majorVersion: Platform.Version,
                },
                countryCode: countryCode,
            });
            const response = await getGlobalConfiguration(body, { courseType: 'BOTH' });
            const payload = GlobalConfigModel(response?.data?.data);
            saveUserGlobalConfig(payload);
        } catch (error) {
        }
    };

    getCommonHeaders = async () => {
        /**
         * Uncomment it if running directly
         */
        const asynCommonHeaders = await getAxiosCommonParams();

        const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblZlcnNpb24iOjgsImlkIjoiZWMyZmIxYzgtODcyYy00NjUzLTlmY2UtZjBiODMyMmViODBmIiwidG9rZW5UeXBlIjoicHJpbWFyeV90b2tlbiIsImV4cCI6MTY0MTkwNDM3NSwiaWF0IjoxNjM0MTI4Mzc1fQ.UCE8psOSewBeh-6B-5VFXFPvmALQzO3rD_qXYsPOsJU"
        let commonHeaders = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
        if (token) {
            commonHeaders = {
                ...commonHeaders,
                authorization: `Bearer ${token}`,
            };
        }
        return asynCommonHeaders ? asynCommonHeaders?.commonHeaders : commonHeaders;
    };

    getCommonQueries = async () => {
        const queryParams = await getAxiosCommonParams();
        return queryParams ? queryParams?.commonQueries : null;
    };

    creatAxiosInstance = async () => {
        this.axiosClient = axios.create({
            baseURL: Configuration.apis.apiBaseUrl,
            timeout: 15000,
        });

        /**
         *Descriptiom: Axios Interceptor for Request
         */
        this.axiosClient.interceptors.request.use(
            async (request) => {
                return request;
            },
            (error) => {
                // Bugsnag.notify(
                //   `Url:${Configuration.apis.apiBaseUrl}${
                //     error?.response?.config?.url
                //   }, Code:${error?.response?.status}, Message:${
                //     error?.response?.data?.error?.message || API_ERROR
                //   }`,
                // );
                // store.dispatch({
                //   type: API_FAILED_ERROR,
                //   payload: error,
                // });
                // hide all loader
                // store.dispatch({
                //   type: GET_LOADER,
                //   payload: false,
                // });
                return Promise.reject(error);
            },
        );

        /**
         *Descriptiom: Axios Interceptor for Response
         */
        this.axiosClient.interceptors.response.use(
            (response) => {
                if (response.status === 200 || response.status === 201) {
                    // reset flag to show global error alert
                    //   store.dispatch({
                    //     type: DONT_SHOW_ERROR_ALERT,
                    //     payload: false,
                    //   });
                    //   calculateAndSaveServerDiff(response?.data?.date);
                    return response;
                }
            },
            (error) => {
                // Bugsnag.notify(
                //   `Url:${Configuration.apis.apiBaseUrl}${
                //     error?.response?.config?.url
                //   }, Code:${error?.response?.status}, Message:${
                //     error?.response?.data?.error?.message || API_ERROR
                //   }`,
                // );
                const payload = error.response ? error.response : null;
                // store.dispatch({
                //   type: API_FAILED_ERROR,
                //   payload: payload,
                // });
                // hide all loader
                // store.dispatch({
                //   type: GET_LOADER,
                //   payload: false,
                // });

                return Promise.reject(error);
            },
        );
    };

    /**
     * Description: Executes a Get Request
     * @param {string} endUrl request Url
     * @param {Object} headers headers specific to current request
     * @param {Object} queryParams request query parameters
     * @returns {Promise} returns a promise of response
     */

    getRequest = async (endUrl, queryParams, headers) => {
        let requestHeaders = await this.getCommonHeaders();
        if (headers) {
            requestHeaders = { ...requestHeaders, ...headers };
        }

        let requestQueries = await this.getCommonQueries();
        if (queryParams) {
            requestQueries = { ...requestQueries, ...queryParams };
        }
        return this.axiosClient.get(endUrl, {
            params: requestQueries,
            headers: requestHeaders,
        });
    };

    /**
     * Description: Executes a Post Request
     * @param {string} endUrl request Url
     * @param {Object} headers headers specific to current request
     * @param {Object} body request body
     * @returns {Promise} returns a promise of response
     */
    postRequest = async (endUrl, body, queryParams, headers, timeoutInms) => {
        let requestHeaders = await this.getCommonHeaders();
        if (headers) {
            requestHeaders = { ...requestHeaders, ...headers };
        }

        let requestQueries = await this.getCommonQueries();
        if (queryParams) {
            requestQueries = { ...requestQueries, ...queryParams };
        }

        if (timeoutInms) {
            return this.axiosClient.post(endUrl, body, {
                params: requestQueries,
                headers: requestHeaders,
                timeout: timeoutInms,
            });
        } else {
            return this.axiosClient.post(endUrl, body, {
                params: requestQueries,
                headers: requestHeaders,
            });
        }
    };

    postRequestWithCustomUrl = async (
        baseUrl,
        endUrl,
        body,
        queryParams,
        headers,
        timeoutInms,
    ) => {
        let requestHeaders = await this.getCommonHeaders();

        let apiClient = axios.create({
            baseURL: baseUrl,
            timeout: 15000,
        });

        if (headers) {
            requestHeaders = { ...requestHeaders, ...headers };
        }

        let requestQueries = await this.getCommonQueries();
        if (queryParams) {
            requestQueries = { ...requestQueries, ...queryParams };
        }

        if (timeoutInms) {
            return apiClient.post(endUrl, body, {
                params: requestQueries,
                headers: requestHeaders,
                timeout: timeoutInms,
            });
        } else {
            return apiClient.post(endUrl, body, {
                params: requestQueries,
                headers: requestHeaders,
            });
        }
    };

    /**
     * Description: Executes a patch Request
     * @param {string} endUrl request Url
     * @param {Object} headers headers specific to current request
     * @param {Object} queryParams request query parameters
     * @returns {Promise} returns a promise of response
     */
    patchRequest = async (endUrl, postParams, queryParams, headers) => {
        let requestHeaders = await this.getCommonHeaders();
        if (headers) {
            requestHeaders = { ...requestHeaders, ...headers };
        }

        let requestQueries = await this.getCommonQueries();
        if (queryParams) {
            requestQueries = { ...requestQueries, ...queryParams };
        }

        return this.axiosClient.patch(endUrl, postParams, {
            params: requestQueries,
            headers: requestHeaders,
        });
    };

    /**
     * Description: Executes a delete Request
     * @param {string} endUrl request Url
     * @param {Object} headers headers specific to current request
     * @param {Object} queryParams request query parameters
     * @returns {Promise} returns a promise of response
     */
    deleteRequest = async (endUrl, queryParams, headers) => {
        let requestHeaders = await this.getCommonHeaders();
        if (headers) {
            requestHeaders = { ...requestHeaders, ...headers };
        }

        let requestQueries = await this.getCommonQueries();
        if (queryParams) {
            requestQueries = { ...requestQueries, ...queryParams };
        }

        return this.axiosClient.delete(endUrl, {
            params: requestQueries,
            headers: requestHeaders,
        });
    };

    /**
     * Description: Executes a Put Request
     * @param {string} endUrl request Url
     * @param {Object} headers headers specific to current request
     * @param {Object} queryParams request query parameters
     * @returns {Promise} returns a promise of response
     */
    putRequest = async (endUrl, postParams, headers) => {
        let requestHeaders = await this.getCommonHeaders();
        if (headers) {
            requestHeaders = { ...requestHeaders, ...headers };
        }

        let requestQueries = await this.getCommonQueries();
        if (postParams) {
            requestQueries = { ...requestQueries, ...postParams };
        }
        return this.axiosClient.put(endUrl, requestQueries, {
            headers: requestHeaders,
        });
    };
}

export const getRequestForGenericAPis = (endUrl, queryParams, headers) => {
    return axios.get(endUrl, { params: queryParams, headers: headers });
};

export const putRequestForGenericAPis = (
    endUrl,
    postParams,
    queryParams = null,
    headers = null,
) => {
    return axios.put(endUrl, postParams, {
        params: queryParams,
        headers: headers,
    });
};
