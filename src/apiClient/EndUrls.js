const endUrls = {
    authenticateCancelOTP: '/userDetail/authenticateToken',
    sendCancleOTP: '/users/sendVerificationCode',
    oneTimeScheduler: (studentId) =>
        `/bookings/students/${studentId}/oneTimeScheduler`,
    avaiableTeacherList: (studentId) =>
        `/students/${studentId}/getSubstituteTeacherForOneTimeScheduler`,
    userConfig: '/userDetail/me',
    geoInfo: '/geo/getInfo',
    globalConfig: '/config-orchestration/getConfigContent',
};

export default endUrls;
