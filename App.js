import React from 'react';
import {
  SafeAreaView,
} from 'react-native';
import ApiClient from './src/apiClient';
import PaidScheduler from './src/views/screens/Scheduler/PaidScheduler'
const apiClient = new ApiClient();
apiClient.creatAxiosInstance();

const App = () => {

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <PaidScheduler />
    </SafeAreaView>
  );
};

export default App;
